import main
import time
import database
import urllib2


class User(object):
    default_data = {'matric_no': '', 'password': '', 'department': '', 'college': '', 'level': '', 'course_history': {},
                    'fullname': '', 'bio': '', 'profile_photo': ''}

    def form(self, dict_):
        dict_['friends'] = []
        dict_['conversations'] = {}
        dict_['bio'] = ''
        dict_['notebooks'] = []
        dict_['notebooks_by_me'] = []
        return dict_

    def head(self, dict_, *include):
        head_ = {}
        head_props = ['matric_no', 'bio', 'fullname', 'department', 'college', 'level', 'profile_photo'] + list(include)

        for prop in head_props:
            head_[prop] = dict_[prop]

        return head_

    def refactor(self, sql_data):
        refined_data = {}

        for key_ in self.default_data:
            if key_ in sql_data:
                if key_ == 'matric_no':
                    refined_data['matric_no'] = sql_data[key_].upper()
                else:
                    refined_data[key_] = sql_data[key_]

            else:
                refined_data[key_] = self.default_data[key_]

        return refined_data


class Note(object):
    def form(self, dict_):
        dict_['created_at'] = time.ctime()
        dict_['liked_by'] = []
        dict_['read_by'] = []
        dict_['comments'] = []
        return dict_


class Notebook(object):
    def form(self, dict_):
        return


class Radar(object):
    default_dict = {'likes': [], 'comments': [], 'created_at': '', 'creator': '',
                    'views': []}

    def generate_id(self, mat_no):
        return 'radar-%s-%s' % (mat_no, time.time())

    def form(self, dict_):
        self.default_dict['title'] = dict_['title']
        self.default_dict['description'] = dict_['description']
        self.default_dict['weblink'] = dict_['weblink']
        self.default_dict['instagram'] = dict_['instagram']
        self.default_dict['created_at'] = time.ctime()
        self.default_dict['creator'] = dict_['creator_header']
        self.default_dict['cover_id'] = dict_['cover_id']
        self.default_dict['id'] = dict_['id']

        return self.default_dict

    def strip(self, id, mat_no):
        radar_ = database.get_radar()
        this_radar_post = radar_[id]

        this_radar_post['viewed'] = False
        if mat_no in this_radar_post['views']:
            this_radar_post['viewed'] = True
        this_radar_post['liked'] = False
        if mat_no in this_radar_post['likes']:
            this_radar_post['liked'] = True

        this_radar_post['views'] = len(this_radar_post['views'])
        this_radar_post['likes'] = len(this_radar_post['likes'])
        this_radar_post['comments'] = len(this_radar_post['comments'])
        this_radar_post['cover'] = '%s/blob?id=%s&content_type=%s' % (main.servers_ip,
                                                                      urllib2.quote(this_radar_post['cover_id']),
                                                                      'image/png')

        return this_radar_post


class Blob(object):
    def generate_id(self):
        return 'blob-%s' % (time.time())


class Message(object):
    default_dict = {'text': '', 'attachment': '', 'sender': '', 'recipient': '', 'status': 'sent'}

    def generate_id(self, sender):
        id = '%s-%s' % (sender, time.time())
        return id

    def form(self, dict):
        for key_ in dict:
            self.default_dict[key_] = dict[key_]

        self.default_dict['id'] = self.generate_id(self.default_dict['sender'])
        return self.default_dict
