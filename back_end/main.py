import webapp2
import calendar
import logging
import os
import sys
import json
import time
import random
import urllib as parser
import argparse
from models import User, Note, Notebook, Radar, Blob, Message
import database
from json import JSONEncoder
from google.storage import speckle
from google.appengine.ext import blobstore, db
from google.appengine.api import mail, app_identity
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import images


if sys.platform == 'win32':
    servers_ip = 'http://localhost:8080' ##feel free to change this to your localhost's port
else:
    servers_ip = 'http://abuadportal-195119.appspot.com'   ## and this to the server's address

import apiserver


class TimetablePage(webapp2.RequestHandler):
    def get(self):
        pass


class StudyTips(webapp2.RequestHandler):
    def get(self):
        study_tips = [
            {
                'title': 'Breaks',
                'body': ('Always take 5-10 minutes break, between 30-45 minutes'
                         ' reading sessions.')
            },

            {
                'title': 'Memorising',
                'body': ('Spend 15 minutes recalling from memory, what you read'
                         ' the previous time.')
            },

            {
                'title': 'Recognition',
                'body': ('The brain recalls faster from smell, than from '
                         'sight.')
            }
        ]

        this_tip = random.choice(study_tips)
        logging.info(this_tip)
        self.response.headers['content_type'] = 'text/json'
        self.response.write(JSONEncoder().encode(this_tip))


class DocumentsPage(webapp2.RequestHandler):
    def get(self):
        mat_no = self.request.get('matric_no')
        action = self.request.get('action')

        if action == 'get documents':
            self.response.headers['content_type'] = 'text/json'
            semester = self.request.get('semester')
            # this gives the documents students have sent
            user_file = database.get_user_file(mat_no)
            atlas = database.get_user_file('atlas')

            sem_cf = user_file['course_history'][semester]
            all_documents = atlas['documents']
            this_documents = {}

            for a_course in sem_cf:
                c_documents = all_documents[semester][a_course['code']]
                this_documents[a_course] = c_documents

            self.response.write(JSONEncoder().encode(this_documents))

        elif action == 'like note':
            pass

        elif action == 'download note':
            self.response.headers['content_type'] = 'text/json'
            author_fullname = self.request.get('')
            notefile = database.get_note_file('notes', author_fullname)
            self.response.write(notefile)

        elif action == 'comment note':
            pass

    def post(self):
        """transcribe audio notes"""
        action = self.request.get('action')

        if action == 'transcribe':
            audio = self.request.get('audio')
            words = ''
            self.response.write(words)

        elif action == 'upload note':
            mat_no = self.request.get('matric_no')
            name = self.request.get('name')
            course_code = self.request.get('course_code')
            file = self.request.get('file')
            semester = self.request.get('semester')

            user_file = database.get_user_file(mat_no)
            author_fullname = user_file['fullname']
            try:
                user_file['documents_by_me'].append('%s_%s' % (name, author_fullname))
            except KeyError:
                user_file['documents_by_me'] = ['%s_%s' % (name, author_fullname)]

            database.write_note_file('notes', author_fullname, file)
            note_dict = Note().form({'name': name, 'author': author_fullname})

            atlas = database.get_user_file('atlas')
            all_documents = atlas['documents']

            try:
                this_semester_nb = all_documents[semester]
            except:
                this_semester_nb = all_documents[semester] = {}
            this_semester_nb[course_code] = note_dict

            database.write_user_file(mat_no, user_file)


class UserProfilePage(webapp2.RequestHandler):
    def get(self):
        return


class ConversationsPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['content_type'] = 'text/json'

        action = self.request.get('action')
        matric_no = self.request.get('matric_no')
        msg_id = self.request.get('msg_id')
        recipient = self.request.get('recipient')

        atlas = database.get_user_file('atlas')

        if action == 'suggestions':
            ret_ = []
            try:
                all_users = atlas['all_users']
            except:
                all_users = []
                ret_ = []

            u_limit = len(all_users)
            if u_limit > 7:
                u_limit = 7

            while len(ret_) < u_limit:
                this_user = random.choice(all_users)
                if this_user == matric_no:
                    u_limit -= 1
                    continue

                user_file = database.get_user_file(this_user)
                dict_ = User().head(user_file)
                dict_['connection'] = 'Let\'s chat'
                ret_.append(dict_)

            self.response.write(JSONEncoder().encode(ret_))

        elif action == 'send_read_receipt':
            msg_file = database.get_message(msg_id)
            msg_file['status'] = 'read'
            database.save_message(msg_id, msg_file)

        elif action == 'get undelivered':
            recipient = self.request.get('recipient')

            user_file = database.get_user_file(matric_no)

            if not 'conversations' in user_file:
                user_file['conversations'] = {}

                database.write_user_file(matric_no, user_file)
                user_file = database.get_user_file(matric_no)

            try:
                user_conversations = user_file['conversations'][recipient]
            except KeyError:
                user_conversations = []

            unread_messages = []

            for message_id in user_conversations:
                message = database.get_message(message_id)

                if message['status'] == 'sent':
                    if message['recipient']['matric_no'] == matric_no:
                        message['status'] = 'delivered'
                    unread_messages.append(message)

                database.save_message(message_id, message)

            self.response.write(JSONEncoder().encode(unread_messages))

        elif action == 'get all conversations':
            user_file = database.get_user_file(matric_no)

            if not 'conversations' in user_file:
                user_file['conversations'] = {}

                database.write_user_file(matric_no, user_file)
                user_file = database.get_user_file(matric_no)

            all_user_conversations = {}

            for recipient in user_file['conversations']:
                try:
                    user_conversations = user_file['conversations'][recipient]
                except KeyError:
                    user_conversations = []

                unread_messages = []

                for message_id in user_conversations:
                    message = database.get_message(message_id)

                    unread_messages.append(message)

                    database.save_message(message_id, message)

                all_user_conversations[recipient] = unread_messages

            self.response.write(JSONEncoder().encode(all_user_conversations))

    def post(self):
        matric_no = self.request.get('matric_no')
        recipient = self.request.get('recipient')
        text = self.request.get('text')
        attachment = self.request.get('attachment')

        attachment_id = None
        if attachment and len(attachment) > 0:
            attachment_id = Blob().generate_id()
            database.add_blob(attachment_id, attachment)

        sender_file = database.get_user_file(matric_no)
        recipient_file = database.get_user_file(recipient)

        sender_head = User().head(sender_file)
        recipient_head = User().head(recipient_file)

        message_dict = Message().form(
            {'sender': sender_head, 'recipient': recipient_head, 'text': text,
             'attachment': attachment_id, 'time': time.ctime()})
        message_id = message_dict['id']
        database.save_message(message_id, message_dict)

        user_file = database.get_user_file(matric_no)
        try:
            user_file['conversations'][recipient].append(message_id)
        except KeyError:
            user_file['conversations'][recipient] = [message_id]
        database.write_user_file(matric_no, user_file)

        recipient_file = database.get_user_file(recipient)
        if 'conversations' not in recipient_file:
            recipient_file['conversations'] = {}

        try:
            recipient_file['conversations'][matric_no].append(message_id)
        except KeyError:
            recipient_file['conversations'][matric_no] = [message_id]
        database.write_user_file(recipient, recipient_file)

        self.response.headers['content-type'] = 'text/json'
        self.response.write(JSONEncoder().encode(user_file['conversations'][recipient]))


class AppHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello from AbuadPortal')


class CreateUserPage(webapp2.RequestHandler):
    def get(self):
        mat_no = self.request.get('matric_no')
        fullname = self.request.get('fullname')
        department = self.request.get('department')
        college = self.request.get('college')

        dict_ = User().form({"mat_no": mat_no, "fullname": fullname,
                             "department": department, "college": college})
        database.write_user_file(mat_no, dict_)

        self.response.headers['content_type'] = 'text/json'
        self.response.write(JSONEncoder().encode(dict_))


class LoginPage(webapp2.RequestHandler):
    def get(self):
        'deprecated... apiserver is called instead'


class APIServer(webapp2.RequestHandler):
    def get(self):
        request = self.request.get('request')

        mat_no, password, course, sem_session, selected_courses, address = (
            self.request.get('matric_no'),
            self.request.get('password'),
            self.request.get('course'),
            self.request.get('sem_session'),
            self.request.get('selected_courses'),
            self.request.get('address')
        )

        if request == 'get_user_file':
            dict_ = apiserver.get_user_file(mat_no, password)
        elif request == 'get_documents':
            dict_ = apiserver.get_documents(mat_no, course)
        elif request == 'get_assignments':
            dict_ = apiserver.get_assignments(mat_no, course)
        elif request == 'get_announcements':
            dict_ = apiserver.get_announcements()
        elif request == 'get_course_tree':
            dict_ = apiserver.get_course_tree(mat_no, sem_session)
        elif request == 'register_courses':
            dict_ = apiserver.register_courses(mat_no, selected_courses, sem_session)
        elif request == 'dowload_blob':
            dict_ = apiserver.download_blob(address)

        self.response.headers['content_type'] = 'text/json'
        self.response.write(JSONEncoder().encode(dict_))


class RadarPage(webapp2.RequestHandler):
    def get(self):
        request = self.request.get('request')
        mat_no = self.request.get('matric_no')

        if request == 'get':
            radar_ = database.get_radar()

            ret_ = []

            for post_ in radar_:
                post_ = Radar().strip(post_, mat_no)
                ret_.append(post_)

            self.response.headers['content_type'] = 'text/json'
            self.response.write(JSONEncoder().encode(ret_))

        elif request == 'like':
            id = self.request.get('id')

            all_radar_posts = database.get_radar()

            this_post = all_radar_posts[id]
            this_post['likes'].append(mat_no)

            all_radar_posts[id] = this_post

            database.write_radar(all_radar_posts)

        elif request == 'view':
            id = self.request.get('id')

            all_radar_posts = database.get_radar()

            this_post = all_radar_posts[id]
            this_post['views'].append(mat_no)

            all_radar_posts[id] = this_post

            database.write_radar(all_radar_posts)

            self.redirect(this_post['weblink'])

        elif request == 'add_page':
            add_html_page = open('webpages/add_page.html', 'r').read()
            the_css = open('webpages/design.css', 'r').read()
            add_html_page = add_html_page.replace('{the_css}', the_css)
            add_html_page = add_html_page.replace('{matric_no}', mat_no)
            self.response.write(add_html_page)

    def post(self):
        title = str(self.request.get('title'))
        description = self.request.get('description')
        weblink = self.request.get('weblink')
        instagram = self.request.get('instagram')
        file = self.request.get('file')
        mat_no = self.request.get('matric_no')

        if not title.isupper():
            title = title.title()

        this_id = Blob().generate_id()
        # file = images.Image(file)
        # file.resize(width=128, height=128)
        file_id = database.add_blob(this_id, file)

        # calling creator_file from our own database, to handle some pressure off their servers
        creator_file = database.get_user_file(mat_no)
        creator_header = User().head(creator_file)

        existing_radar = database.get_radar()
        if not existing_radar:
            existing_radar = {}

        new_id = Radar().generate_id(mat_no)
        existing_radar[new_id] = Radar().form(
            {'title': title, 'description': description, 'weblink': weblink,
             'instagram': instagram, 'creator_header': creator_header,
             'cover_id': file_id, 'id': new_id})
        database.write_radar(existing_radar)

        self.response.headers['content-type'] = 'image/png'
        self.response.write(file_id)
        self.response.write(file)


class BlobPage(webapp2.RequestHandler):
    def get(self):
        fn = self.request.get('fn')
        content_type = str(self.request.get('content_type'))

        fn = fn.lower()
        fn = ''.join(fn.split(' '))

        self.response.headers['content-type'] = content_type
        fd = open('/fake_blob/%s' % fn, 'r').read()
        self.response.write(fd)
        # self.response.write(database.get_blob(fn))


class BotPage(webapp2.RequestHandler):
    def get(self):
        sub = self.request.get('sub')
        fb_id = self.request.get('fb_id')

        self.response.headers['content-type'] = 'text/json'

        if sub == 'get_bot_file':
            bot_file = database.get_bot_file(fb_id)
            # chat_history = bot_file['chat_history']
            bot_file = JSONEncoder().encode(bot_file)
            self.response.write(bot_file)

    def post(self):
        sub = self.request.get('sub')
        fb_id = self.request.get('fb_id')
        new_file = self.request.get('new_file')

        if sub == 'write_bot_file':
            # old_file = database.get_bot_file(fb_id)
            # old_file['chat_history'] = new_file
            database.write_bot_file(fb_id, new_file)


urlMappings = [('/', AppHandler), ('/conversations', ConversationsPage),
               ('/profile', UserProfilePage), ('/documents', DocumentsPage),
               ('/study_tips', StudyTips), ('/timetable', TimetablePage),
               ('/create_user', CreateUserPage), ('/login', LoginPage),
               ('/api', APIServer), ('/radar', RadarPage), ('/blob', BlobPage),
               ('/bot', BotPage)]

app = webapp2.WSGIApplication(urlMappings, debug=True)
