# to be placed on our servers
# basic function is to query their database, get whatever information we need, and send it to us

# import sqlite3
import time
import logging
import database
import main
from models import *

example_course = {'code': 'EEE 341', 'title': '', 'units':'3', 'status':'R'}
example_assignments = {'code': 'EEE 314', 'question': '', 'deadline': '', 'blob': ''}
example_announcement = {'message': '', 'author': '', 'time': '', 'venue': ''}

fake_database = {'users': {'12/SCI14/000':
                               {'matric_no': '12/SCI14/000', 'fullname': 'Soloman Grandi', 'password': 'passworder',
                                'department': 'Electrical/Electronics Engineering', 'college': 'Engineering',
                                'level': '500', 'profile_photo': '%s/assets/default_profile_photo.png' %
                                                                 main.servers_ip,
                                'course_history':
                                    {'first 2017/2018':
                                         [{'code': 'EEE 401', 'title': 'Introduction to Electronics', 'units': 3},
                                          {'code': 'EEE 403', 'title': 'Fundamentals of Computing', 'units': 2}]
                                    }
                                },

                          '15/ENG02/999':
                               {'matric_no': '15/ENG02/999', 'fullname': 'Mister Jackson', 'password': 'passworder',
                                'department': 'Computer Engineering', 'college': 'Engineering', 'level': '200',
                                'profile_photo': '%s/assets/default_profile_photo.png' % main.servers_ip,
                                'course_history':
                                    {'first 2017/2018':
                                         [{'code': 'EEE 401', 'title': 'Introduction to Electronics', 'units': 3},
                                          {'code': 'EEE 403', 'title': 'Fundamentals of Computing', 'units': 2}]
                                    }
                                },

                          '15/MHS04/020':
                               {'matric_no': '15/MHS04/020', 'fullname': 'Mike Corden', 'password': 'passworder',
                                'department': 'Medicine & Surgery', 'college': 'Engineering',
                                'level': '500', 'profile_photo': '%s/assets/default_profile_photo.png' %
                                                                 main.servers_ip,
                                'course_history':
                                    {'first 2017/2018':
                                         [{'code': 'EEE 401', 'title': 'Introduction to Electronics', 'units': 3},
                                          {'code': 'EEE 403', 'title': 'Fundamentals of Computing', 'units': 2}]
                                    }
                                },

                          '15/MHS01/071':
                               {'matric_no': '15/MHS01/071', 'fullname': 'Mackie Babalayo', 'password': 'passworder',
                                'department': 'Anatomy', 'college': 'Medicine & Health Sciences', 'level': '200',
                                'profile_photo': '%s/assets/default_profile_photo.png' % main.servers_ip,
                                'course_history':
                                    {'first 2017/2018':
                                         [{'code': 'EEE 401', 'title': 'Introduction to Electronics', 'units': 3},
                                          {'code': 'EEE 403', 'title': 'Fundamentals of Computing', 'units': 2}]
                                    }
                                },
                          },

                 'announcements': [{'message':'Everyone should be at the hall by 12:00noon today.',
                                    'author': 'Engr. Fola Ade',
                                    'time': time.ctime(), 'venue': 'Alfa Begore Hall'},
                                   {'message': 'Submit your course forms today.',
                                    'author': 'Mrs. Olatito Johnson',
                                    'time': time.ctime(), 'venue': 'Alfa Begore Hall'},
                                   {'message':'Stop ordering from internet stores.',
                                    'author': 'Management',
                                    'time': time.ctime(), 'venue': ''}
                                   ],

                 'assignments': {'EEE 401': [{'code': 'EEE 401', 'question': 'Assignment 1', 'deadline': time.ctime(),
                                              'blob': '', 'lecturer': 'Dr. Yusuff Michael'},
                                             {'code': 'EEE 401', 'question': 'Assignment 2', 'deadline': time.ctime(),
                                              'blob': '', 'lecturer': 'Prof. A.A. Danladi'}
                                             ],
                                'EEE 403': [{'code': 'EEE 403', 'question': 'Assignment ...1', 'deadline': time.ctime(),
                                              'blob': '', 'lecturer': 'Dr. Yusuff Michael'},
                                            {'code': 'EEE 403', 'question': 'Assignment ...2', 'deadline': time.ctime(),
                                              'blob': '', 'lecturer': 'Prof. A.A. Danladi'}
                                             ]
                                 },

                 'course tree': [{'code': 'MAT 101', 'title': 'General Mathematics I', 'units':'3'},
                                 {'code': 'MAT 102', 'title': 'General Mathematics II', 'units':'3'},
                                 {'code': 'MAT 104', 'title': 'General Mathematics IV', 'units':'3'},
                                 {'code': 'STA 111', 'title': 'Statistics I', 'units':'3'},
                                 {'code': 'STA 132', 'title': 'Statistics II', 'units':'3'},
                                 {'code': 'ENG 301', 'title': 'Engineering I', 'units':'3'},
                                 {'code': 'ENG 302', 'title': 'Engineering II', 'units':'3'},
                                 {'code': 'ENG 304', 'title': 'Engineering IV', 'units':'3'},
                                 {'code': 'ENG 281', 'title': 'Engineering Mathematics I', 'units':'3'},
                                 {'code': 'ENG 282', 'title': 'Engineering Mathematics II', 'units':'3'},
                                 {'code': 'ENG 284', 'title': 'Engineer in the Society', 'units':'3'},
                                 {'code': 'ENG 231', 'title': 'Engineering Drawing I', 'units':'2'},
                                 {'code': 'ENG 232', 'title': 'Engineering Drawing II', 'units':'2'}],

                 'documents': {'ENG 302': [{'title': 'Introduction to Spotlight.pdf',
                                            'weblink': 'http://localhost:8080/blob?fn=Introduction+to+Spotlight.pdf&content_type=file/pdf',
                                            'author': {'name': 'Mr. A. A. Danladi', 'post': 'staff'},
                                            'datetime': time.ctime()},

                                           {'title': 'Signals and Waves.pdf',
                                            'weblink': 'http://localhost:8080/blob?fn=Signals+and+Waves.pdf&content_type=file/pdf',
                                            'author': {'name': 'Mr. A. A. Danladi', 'post': 'staff'},
                                            'datetime': time.ctime()},

                                           {'title': 'Differentiation and Integration.pdf',
                                            'weblink': 'http://localhost:8080/blob?fn=Differentiation+and+Integration.pdf&content_type=file/pdf',
                                            'author': {'name': 'Dr. A. A. Danladi', 'post': 'staff'},
                                            'datetime': time.ctime()}
                                           ],
                               'ENG 304': [{'title': 'Tutorial 1.pdf', 'weblink': '',
                                            'author': {'name': 'Mr. A. A. Danladi', 'post': 'staff'},
                                            'datetime': time.ctime()}]}
                 }


def create_user_in_database(mat_no, user_file_refactored):
    """This just clones the user from ABUAD database into our servers"""
    atlas = database.get_user_file('atlas')

    if atlas is None:
        atlas = {}
    # atlas['all_users'] = []
    try:
        atlas['all_users'].append(mat_no)
    except (KeyError, TypeError):
        atlas['all_users'] = [mat_no]
    database.write_user_file('atlas', atlas)
    database.write_user_file(mat_no, user_file_refactored)


def get_user_file(mat_no, password):
    user_file = fake_database['users'][mat_no]
    user_file_refactored = User().refactor(user_file)

    if password and user_file_refactored['password'] == password:
        create_user_in_database(mat_no, user_file_refactored)
        return user_file_refactored


def get_documents(matric_no, course):
    document_list = fake_database['documents'][course]
    return document_list


def get_assignments(matric_no, course):
    try:
        return fake_database['assignments'][course]
    except:
        return []


def get_announcements():
    return fake_database['announcements']


def get_course_tree(mat_no, sem_session):
    ct = fake_database['course tree']
    ct.sort()

    user_file = database.get_user_file(mat_no)
    try:
        this_cf = user_file['course_history'][sem_session]
    except:
        this_cf = []
    # this_cf = user_file['course_history'][sem_session] = []
    # database.write_user_file(mat_no, user_file)

    logging.info('User\'s course_form this semester: %s' % this_cf)

    for course in ct:
        course_code = course['code']

        code = int(course_code.split(' ')[1])
        if code % 2 == 0 and sem_session.startswith('first'):
            ct.remove(course)
        elif code % 2 != 0 and sem_session.startswith('second'):
            ct.remove(course)

        # course_status = course.pop('status')
        if 'registered' in course:
            course.pop('registered')

        if course in this_cf and type(course) is dict:
            ct[ct.index(course)]['registered'] = True

        # course['status'] = course_status

    return ct


def register_courses(mat_no, selected_courses, sem_session):
    user_file = database.get_user_file(mat_no)
    try:
        this_cf = user_file['course_history'][sem_session]
    except:
        this_cf = user_file['course_history'][sem_session] = []

    this_cf = eval(selected_courses)
    user_file['course_history'][sem_session] = this_cf
    database.write_user_file(mat_no, user_file)

    # also save the this_cf into school's database

    return user_file['course_history'][sem_session]


def get_course_form(mat_no, sem_session):
    existing_cf = fake_database[mat_no]['course_history'][sem_session]
    try:
        this_cf = user_file['course_history'][sem_session]
    except:
        this_cf = user_file['course_history'][sem_session] = []

    this_cf = eval(selected_courses)
    user_file['course_history'][sem_session] = this_cf
    database.write_user_file(mat_no, user_file)


def download_blob(address):
    blob = 'some database extract goes here'
    return blob