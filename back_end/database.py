from google.appengine.ext import db
import random
import time
import os


class UserDB(db.Expando):
    pass


class BotDB(db.Expando):
    pass


class NotesDB(db.Expando):
    pass


class RadarDB(db.Expando):
    pass


class BlobsDB(db.Expando):
    pass


class MessagesDB(db.Expando):
    pass


def get_user_file(mat_no):
    k = db.Key.from_path('UserDB', mat_no)
    e = db.get(k)

    if e:
        return eval(e.dict_)


def write_user_file(mat_no, file):
    k = db.Key.from_path('UserDB', mat_no)
    e = db.get(k)
    try:
        e.dict_ = db.Text(str(file))
        e.put()
    except:  # create new file
        this_user = UserDB(key_name=mat_no)
        this_user.username = mat_no
        this_user.dict_ = db.Text(str(file))
        this_user.put()


def get_bot_file(fb_id):
    k = db.Key.from_path('BotDB', fb_id)
    e = db.get(k)

    if e:
        return eval(e.dict_)


def write_bot_file(fb_id, user_dict):
    k = db.Key.from_path('BotDB', fb_id)
    e = db.get(k)
    try:
        e.dict_ = db.Text(str(file))
        e.put()
    except:  # create new file
        this_user = BotDB(key_name=fb_id)
        this_user.username = fb_id
        this_user.dict_ = db.Text(str(user_dict))
        this_user.put()


def get_radar():
    k = db.Key.from_path('RadarDB', 'all')
    e = db.get(k)

    if e:
        return eval(e.dict_)


def write_radar(new_dict):
    k = db.Key.from_path('RadarDB', 'all')
    e = db.get(k)

    try:
        e.dict_ = db.Text(str(new_dict))
        e.put()
    except:  # create new file
        this_user = RadarDB(key_name='all')
        this_user.username = 'all'
        this_user.dict_ = db.Text(str(new_dict))
        this_user.put()


def get_note_file(name, author):
    fn = '%s_%s' % (name, author)
    k = db.Key.from_path('NotesDB', fn)
    e = db.get(k)

    if e:
        return bytes(e.file)


def write_note_file(name, author, file):
    fn = '%s_%s' % (name, author)
    k = db.Key.from_path('NotesDB', fn)
    e = db.get(k)
    try:
        e.dict_ = db.Text(str(file))
        e.put()
    except:  # create new file
        this_note = NotesDB(key_name=fn)
        this_note.name = name
        this_note.author = author
        this_note.file = db.Text(str(file))
        this_note.put()


def gcs_write(filename, newContents):
    import cloudstorage as gcs
    from google.appengine.api import app_identity

    write_retry_params = gcs.RetryParams(backoff_factor=11.1)
    gcs_file = gcs.open(filename,
                        'w',
                        content_type='text/plain',
                        options={'x-goog-meta-foo': 'foo',
                                 'x-goog-meta-bar': 'bar'},
                        retry_params=write_retry_params)
    gcs_file.write(str(newContents))
    gcs_file.close()


def gcs_write_file(filename, newContents, mimeType):
    import cloudstorage as gcs
    from google.appengine.api import app_identity

    write_retry_params = gcs.RetryParams(backoff_factor=11.1)
    gcs_file = gcs.open(filename,
                        'w',
                        content_type=mimeType,
                        options={'x-goog-meta-foo': 'foo',
                                 'x-goog-meta-bar': 'bar'},
                        retry_params=write_retry_params)
    gcs_file.write(newContents)
    gcs_file.close()


def gcs_read(filename, ):
    import cloudstorage as gcs
    from google.appengine.api import app_identity

    try:
        gcs_file = gcs.open(filename)
        fileContents = gcs_file.read()
        gcs_file.close()

    except:
        ##run self.gcsRestore here
        try:
            gcs_file = gcs.open('%s_backup' % filename)
            fileContents = gcs_file.read()
            gcs_file.close()
        except:
            fileContents = '{}'

    return fileContents


def add_blob(filename, filedata):
    # import cloudstorage as gcs
    # from google.appengine.api import app_identity
    # bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
    #
    # # blob_filename = 'abuad_portal_blobs'
    #
    # # try:
    # #     old_blob_db = eval(gcs_read('/%s/%s' % (bucket_name, blob_filename)))
    # #     old_blob_db[imgName] = imgFile
    # # except:
    # #     old_blob_db = eval(gcs_read('/%s/%s' % (bucket_name, blob_filename)))
    # #     old_blob_db[imgName] = imgFile
    # #
    # # gcs_write('/%s/%s' % (bucket_name, blob_filename), old_blob_db)
    #
    # try:
    #     gcs_write_file('/%s/%s' % (bucket_name, imgName), imgFile, 'image/png')
    # except:
    #     gcs_write_file('/%s/%s' % (bucket_name, imgName), imgFile, 'image/jpeg')
    #
    # return imgName

    k = db.Key.from_path('BlobsDB', filename)
    e = db.get(k)

    try:
        e.dict_ = db.Text(str(filedata))
        e.put()

    except:  # create new file
        this_user = BlobsDB(key_name=filename)
        this_user.filename = filename
        this_user.file = db.Blob(filedata)
        this_user.put()

    return filename


def get_blob(filename):
    # from google.appengine.api import app_identity
    # bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())
    #
    # # blob_filename = 'abuad_portal_blobs'
    #
    # old_blob_db = gcs_read('/%s/%s' % (bucket_name, imgName))
    # return old_blob_db

    k = db.Key.from_path('BlobsDB', filename)
    e = db.get(k)

    if e:
        return e.file


def save_message(message_id, message_dict):
    k = db.Key.from_path('MessagesDB', message_id)
    e = db.get(k)

    try:
        e.dict_ = db.Text(str(message_dict))
        e.put()

    except:  # create new file
        this_user = MessagesDB(key_name=message_id)
        this_user.message_id = message_id
        this_user.dict_ = db.Text(str(message_dict))
        this_user.put()


def get_message(message_id):
    k = db.Key.from_path('MessagesDB', message_id)
    e = db.get(k)

    if e:
        return eval(e.dict_)
