import urllib as parser


class FacebookREST(object):

    def visit_fb(self, values__, url, path):
        coded_data = parser.urlencode(values__)

        coded_data = coded_data.encode('utf8')
        # requester.Request(url, codedData__, {})

        import httplib
        conn = httplib.HTTPSConnection(url)
        conn.request("POST", path, coded_data)
        response = conn.getresponse()

        data = response.read()
        conn.close()

        return data, response.reason, response.status

    def publish(self, dict_):
        message = dict_['message']
        link = dict_['link']
        access_token = dict_['access_token']

        values__ = {'message': message, 'link': link,
                    'access_token': access_token}
        return self.visit_fb(values__, 'graph.facebook.com', "/v2.10/me/feed")

    def get_user(self, access_token):
        values__ = {'access_token': access_token}
        return self.visit_fb(
            values__,
            'graph.facebook.com',
            "/v2.10/me?fields=id,name,email,friends,gender,birthday,about,cover"
        )
