__author__ = 'Tomisin Abiodun'
__title__ = 'ABUAD Portal'

import os

if not os.path.exists('/sdcard/abuadportal'):
    os.mkdir('/sdcard/abuadportal')

from kivy.config import Config
Config.set('kivy', 'log_dir', '/sdcard/abuadportal')


import sys
import logging
import time
import calendar
import threading
import getpass
import webengine
from notification import Notification
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.scrollview import ScrollView
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.image import AsyncImage, Image
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.core.window import Window
from kivy.atlas import Atlas
from kivy.core.audio import SoundLoader
from kivy.uix.switch import Switch
from kivy.storage.jsonstore import JsonStore
from kivy.uix.spinner import Spinner, SpinnerOption
from kivy.uix.carousel import Carousel
from kivy.animation import Animation
from kivy.uix.checkbox import CheckBox
from kivy.utils import platform, get_color_from_hex, get_hex_from_color, get_random_color
from kivy.metrics import cm


atlas = Atlas('trail.atlas')


notification_sound = SoundLoader.load('notificationsound.wav')
notification = Notification(sound=notification_sound)

Clock.max_iteration += 1000

# Window.size = (490, 700)

mobile = False

if Window.width < Window.height:
    mobile = True
else:
    Window.maximize()

download_path = '/sdcard/abuadportal/downloads'
if platform == 'win':
    user = getpass.getuser()
    download_path = 'C://Users/%s/abuadportal/downloads' % user

blob_path = 'blobs'

subtext_color = '606060'

for a_path in [download_path, blob_path]:
    if not os.path.exists(a_path):
        os.mkdir(a_path)


def check_size(width, height):
    if sys.platform == 'win32':
        Window.maximize()

if not mobile:
    Window.on_resize = check_size


def font_scale(int_):
    win_height = Window.height
    if win_height >= 1900:
        multiplier = float(win_height) / 850
        return int_ * int(round(multiplier))

    elif win_height > 1000:
        multiplier = float(win_height) / 785
        return int(round(int_ * multiplier))

    else:
        return int(round(int_ * 1))

try:
    membank = JsonStore('membank.json')
except:
    os.remove('membank.json')
    membank = JsonStore('membank.json')


screenman_ = ScreenManager(pos_hint={'center_x': .5, 'center_y': .5})


d_blue = '0881ae'
d_gold = 'd9b408'
d_red = 'bf180b'
app_pri_fg_color = blue = '18a1ce'
app_ter_fg_color = gold = 'e9c418'
app_sec_fg_color = red = 'df382b'


small_font = font_scale(14)
smid_font = font_scale(15)
mid_font = font_scale(17)
big_font = font_scale(21)
bigger_font = font_scale(26)
biggest_font = font_scale(35)
giant_font = font_scale(45)


light = 'assets/Roboto-Light.ttf'
bold = 'assets/Roboto-Bold.ttf'
college = 'assets/college.ttf'

global active_text_input, the_current_p, screen_history
active_text_input = []
the_current_p = []
screen_history = []

Window.clearcolor = (.92, .92, .92, 1)

school_name = 'ABUAD'


def reload_tables():
    membank.put('cache', cache={'colleges': ['Arts & Humanities', 'Medicine & Health Sciences', 'Sciences',
                                             'Engineering', 'Law', 'Social & Management Sciences'],

                                'department_tree': {'Law': ['Law'],
                                                    'Engineering': ['Chemical', 'Civil', 'Computer',
                                                                    'Electrical & Electronic', 'Mechanical',
                                                                    'Mechatronics', 'Petroleum'],
                                                    'Sciences': ['Computer Sciences', 'Physics'],
                                                    'Arts & Humanities': ['English'],
                                                    'Medicine & Health Sciences': [],
                                                    'Social & Management Sciences': []},

                                'college_duration': {'Law': 5, 'Engineering': 5, 'Sciences': 4,
                                                     'Arts & Humanities': 4, 'Social & Management Sciences': 4,
                                                     'Medicine & Health Sciences': 6}
                                })
    membank.put('course_forms', course_forms={})
    membank.put('study_tips', study_tips=[])
    membank.put('friends', friends=[])
    membank.put('documents', documents=[])
    membank.put('radar', radar=[])

try:
    membank.get('cache')['cache']
except:
    reload_tables()


class TextInput(TextInput):
    def __init__(self, **kwargs):

        self.keyboard_suggestions = False

        super(TextInput, self).__init__(**kwargs)


class AsyncImage(AsyncImage):
    def __init__(self, **kwargs):
        super(AsyncImage, self).__init__(**kwargs)

        self.kwargs_ = kwargs
        self.mipmap = True
        self.keep_data = False  #  self.nocache = True
        self.allow_enlarge = 'allow_enlarge' in list(self.kwargs_.keys())
        self.action = None

        if self.allow_enlarge and self.kwargs_['allow_enlarge'] == False:
            self.allow_enlarge = False

        if 'action' in self.kwargs_:
            self.action = self.kwargs_['action']

        # if self.kwargs_['source_'].startswith('http://') or self.kwargs_['source_'].startswith('https://'):
        #     img_name = self.kwargs_['source_'].split('/')[-1]
        #
        #     if os.path.exists('blobs/%s' % img_name):
        #         self.source = 'blobs/%s' % img_name
        #     else:
        #         img_data = webengine.download_blob('', self.kwargs_['source_'])
        #         open('blobs/%s' % img_name, 'wb').write(img_data)
        #         self.source = 'blobs/%s' % img_name
        #
        # else:
        #     self.source = kwargs['source_']

        try:
            if (self.kwargs_['source'].endswith('_dp.jpg') or self.kwargs_['source'].endswith('_cover.jpg')) and \
                            'retain_size' not in self.kwargs_:
                if os.path.exists(self.kwargs_['source']) and os.stat(self.kwargs_['source'])[6] != 0:
                    theFileName_ = self.kwargs_['source'].split('.')[0]
                    if os.path.exists('%s_small.jpg' % theFileName_):
                        self.source = '%s_small.jpg' % theFileName_
                        self.reload()

                        fprint('new size: %s_small.jpg')

        except:
            pass

        try:
            self._coreimage.bind(on_load=self.on_image_loaded)
        except AttributeError:
            pass

    def on_image_loaded(self, *args):
        def onImageLoadedThread():
            if True:
                try:
                    if (self.kwargs_['source'].endswith('_dp.jpg') or self.kwargs_['source'].endswith('_cover.jpg')) and 'retain_size' not in self.kwargs_:
                        if os.path.exists(self.kwargs_['source']) and os.stat(self.kwargs_['source'])[6] != 0:
                            theFileName_ = self.kwargs_['source'].split('.')[0]
                            newRatio = (160, 160)
                            if os.path.exists('%s_small.jpg' % theFileName_):
                                self.source = '%s_small.jpg' % theFileName_
                                return

                            try:
                                from PIL import Image as IMAGE
                                fn = self.kwargs_['source']
                                im = IMAGE.open(fn)

                                im = im.resize(newRatio, IMAGE.ANTIALIAS)

                                im.save('%s_small.jpg' % theFileName_, optimize=True, quality=85)
                                self.source = '%s_small.jpg' % theFileName_
                                self.reload()

                                fprint('new size: %s_small.jpg')

                            except:
                                os.system(
                                    r'C:\Python33\python.exe C:\Users\Tomisin-PC\Desktop\CodeLexis\Services\Trail\Android\Trail_Android\ImageManipulator.py resize "%s" "%s" "%s"' % (
                                    self.kwargs_['source'], newRatio, '%s_small.jpg' % theFileName_))
                                self.source = '%s_small.jpg' % theFileName_
                                self.reload()

                                fprint('new size: %s_small.jpg')

                except KeyError:
                    fprint('Not yet loaded')

        threading.Timer(0, onImageLoadedThread).start()

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            if self.action:
                self.action(self)

            if 'with_list' in list(self.kwargs_.keys()):
                thisGrid = GridLayout(rows=1, spacing=7)
                thisGrid.bind(minimum_height=thisGrid.setter('height'))

                for imgW in self.kwargs_['with_list']:
                    dSrc = imgW.source
                    thisGrid.add_widget(AsyncImage(allow_stretch=True, keep_ratio=False, source=dSrc))

                thisScroll = ScrollView(do_scroll_y=False, size_hint=(.9, .9))
                thisScroll.add_widget(thisGrid)

                thisGrid.size_hint_x = len(thisGrid.children) * .875
                thisScroll.size_hint_x += .0001

                x_ = round(float(touch.x / Window.width))
                y_ = round(float(touch.y / Window.height))
                quickPV = PeoplePopup(content=thisScroll, title='', size_hint=(0, 0), separator_color=(0, 0, 0, 0),
                                      pos_hint={'center_x': x_, 'center_y': y_})
                quickPV.open()

                anim_ = Animation(duration=(.2), size_hint=(1, 1), pos_hint={'center_x': .5, 'center_y': .55})
                anim_.start(quickPV)

            elif self.allow_enlarge:
                global PhotoViewer_
                try:
                    fprint(PhotoViewer_)
                except NameError:
                    PhotoViewer_ = PhotoViewer()
                    screenman_.add_widget(PhotoViewer_)

                try:
                    if os.path.exists(self.source):
                        screenman_.transition = RiseInTransition(duration=0.1)
                        screenman_.current = 'Photo Viewer'
                        screenman_.transition = SlideTransition(duration=.1)

                        if 'caption' in self.kwargs_:
                            Clock.schedule_once(lambda dt: viewPhoto(self.source, self.kwargs_['caption']), -1)
                        else:
                            Clock.schedule_once(lambda dt: viewPhoto(self.source, ''), -1)
                except TypeError:
                    pass

                fprint(self.source)


class Screen(Screen):
    def __init__(self, **kwargs):
        super(Screen, self).__init__(**kwargs)

    def on_enter(self):
        fprint('Entering %s' % self.name)
        if len(screen_history) > 0 and screen_history[-1] == 'Chat Screen':
            clear_chat_screen()

        screen_history.append(self.name)

    def on_exit(self):
        fprint('Leaving screen %s'%self.name)


class BlankPopup(Popup):
    def __init__(self, **kwargs):
        super(BlankPopup, self).__init__(**kwargs)

        self.title = ''
        self.separator_color = (0, 0, 0, 0)

BlankPopup.background = 'atlas://trail/blank'


class PeoplePopup(Popup):
    def __init__(self, **kwargs):
        super(PeoplePopup, self).__init__(**kwargs)
        try:
            self.title = kwargs.pop('title')
        except:
            pass
        try:
            self.content = kwargs.pop('content')
        except:
            pass
        try:
            self.separator_color = kwargs.pop('separator_color')
        except:
            pass
        try:
            self.pos_hint = kwargs.pop('pos_hint')
        except:
            pass
        try:
            self.size_hint = kwargs.pop('size_hint')
        except:
            pass
        try:
            self.background_color = kwargs.pop('background_color')
        except:
            pass

        if self not in the_current_p:
            the_current_p.append(self)

    def on_dismiss(self):
        # if len(the_current_p) > 1:
        #     the_current_p = the_current_p[-2:]

        while self in the_current_p:
            global the_current_p
            try:
                the_current_p.remove(self)
            except:
                pass
        pass

PeoplePopup.background = 'atlas://trail/bigPhotoHolder'


class TabsHolder(BoxLayout):
    def update(self, dict_):
        for kw in dict_:
            eval('self.%s = %s' % (kw, dict_[kw]))

    def get_current_tab(self):
        for butt in self.theTitleBar.children:
            if butt.background_color == self.kwargs['active_color']:
                return butt.text

    def select(self, title):
        index = -1

        for butt in self.theTitleBar.children:
            butt.background_color = self.inactive_color
            butt.font_name = light
            butt.color = (1, 1, 1, 1)

            if butt.text == title:
                butt.color = self.active_fg
                butt.background_color = self.active_color
                index = self.theTitleBar.children.index(butt)

                #butt.on_press()
                self.theCarousel.load_slide(self.theCarousel.slides[-1 -index])

                butt.on_release()

            index -= 1

        self.theTitleBarS.scroll_x = -(index) * self.head_size_x

    def __init__(self, **kwargs):
        super(TabsHolder, self).__init__(**kwargs)

        self.kwargs = kwargs
        self.orientation = 'vertical'
        thisTH = self

        self.active_fg = (0, 0, 0, 1)
        if 'active_fg' in kwargs:
            self.active_fg = kwargs['active_fg']

        self.inactive_color = kwargs['inactive_color']
        self.active_color = kwargs['active_color']
        self.head_size_x = kwargs['head_sizeX']

        try:
            head_padding = kwargs['head_padding']
        except:
            head_padding = [0, 5, 0, 0]

        try:
            head_spacing = kwargs['head_spacing']
        except:
            head_spacing = 1.5

        self.theTitleBar = GridLayout(rows=1, padding=head_padding, spacing=head_spacing)
        self.theTitleBar.bind(minimum_height=self.theTitleBar.setter('height'))

        self.theTitleBarS = ScrollView(do_scroll_y=False, size_hint_y=kwargs['head_sizeY'])
        self.theTitleBarS.add_widget(self.theTitleBar)
        self.add_widget(self.theTitleBarS)

        self.theCarousel = Carousel(anim_move_duration=0.15, scroll_timeout=0)
        self.add_widget(self.theCarousel)

        def showThisTab(self):
            for tabHead in thisTH.theTitleBar.children:
                tabHead.background_color = kwargs['inactive_color']
                tabHead.font_name = light
                tabHead.color = (1, 1, 1, 1)

            self.background_color = kwargs['active_color']
            self.font_name = bold
            self.color = kwargs['active_fg']

            listOfSibblings = thisTH.theTitleBar.children
            listOfSibblings.reverse()

            newTab = listOfSibblings.index(self)
            thisTH.theCarousel.load_slide(thisTH.theCarousel.slides[newTab])

            listOfSibblings.reverse()

        global addChildTab_TH

        def addChildTab_TH(a):
            thisTabHead = Button(text=a['name'], font_name=light, font_size=kwargs['head_fontSize'],
                                 background_normal=kwargs['head_background'], background_down='white',
                                 background_color=kwargs['inactive_color'], color=kwargs['font_color'], halign='center',
                                 on_press=showThisTab, shorten=True, shorten_from='center',
                                 text_size=(.9 * kwargs['head_sizeX'] * Window.width, None))
            if a['on_load']:
                thisTabHead.on_release=a['on_load']

            self.theTitleBar.add_widget(thisTabHead, )
            try:
                a['wid'].parent.clear_widgets()
            except:
                pass
            self.theCarousel.add_widget(a['wid'])

            self.theTitleBar.size_hint_x = len(self.theTitleBar.children) * kwargs['head_sizeX']
            self.theTitleBarS.scroll_x += 0.00001

        for a in kwargs['slides']:
            addChildTab_TH(a)

        self.theTitleBar.children[-1].on_release()
        self.theTitleBar.children[-1].background_color = kwargs['active_color']
        self.theTitleBar.children[-1].font_name = bold
        self.theTitleBar.children[-1].color = self.active_fg
        # try:
        #     self.theTitleBar.children[len(self.theTitleBar.children) - 1].background_color = kwargs['active_color']
        #     self.theTitleBar.children[len(self.theTitleBar.children) - 1].font_name = 'Roboto-Regular.ttf'
        # except:
        #     pass

        self.theTitleBar.size_hint_x = len(self.theTitleBar.children) * self.head_size_x
        # self.theTitleBar.width = cm)


class Time(object):
    def __init__(self):
        self.refresh()

    def refresh(self, *ctime):
        if len(ctime) == 0:
            self.fullDateAndTime = time.ctime()
        else:
            self.fullDateAndTime = ctime[0]
        self.fullDateAndTimeSplitted = self.fullDateAndTime.split(' ')

        try:
            self.fullDateAndTimeSplitted.remove('')
        except:
            pass

        daysInFull = {'Sun': 'Sunday', 'Mon': 'Monday', 'Tue': 'Tuesday', 'Wed': 'Wednesday', 'Thu': 'Thursday',
                      'Fri': 'Friday', 'Sat': 'Saturday'}
        monthsInFull = {'Jan': 'January', 'Feb': 'February', 'Mar': 'March', 'Apr': 'April', 'May': 'May',
                        'Jun': 'June',
                        'Jul': 'July', 'Aug': 'August', 'Sep': 'September', 'Oct': 'October', 'Nov': 'November',
                        'Dec': 'December'}
        self.weekdaysFull = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        self.weekdaysShort = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        self.monthsDays = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30,
                           'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}

        self.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

        self.dayNoAtt = 'th'
        self.dayShort = self.fullDateAndTimeSplitted[0]
        self.monthShort = self.fullDateAndTimeSplitted[1]
        self.dayNo = str(int(self.fullDateAndTimeSplitted[2]))
        self.fullTime = self.fullDateAndTimeSplitted[3]
        self.yearNo = self.fullDateAndTimeSplitted[4]

        self.MM, self.DD = self.months.index(self.monthShort) + 1, self.dayNo
        if len(str(self.MM)) == 1:
            self.MM = '0%s' % self.MM
        if len(str(self.DD)) == 1:
            self.DD = '0%s' % self.DD

        self.dateIndex = '%s%s%s' % (self.yearNo, self.MM, self.DD)

        self.thisHourShort = self.thisHour = int(self.fullTime.split(':')[0])
        self.thisMin = self.fullTime.split(':')[1]

        self.timeP = 'morning'
        if self.thisHour >= 12:
            self.timeP = 'afternoon'
        if self.thisHour >= 16:
            self.timeP = 'evening'

        self.AMPM = 'am'
        if int(self.thisHour) >= 12:
            self.AMPM = 'pm'
            self.thisHourShort = int(self.thisHour) - 12

        if self.thisHourShort == 0:
            self.thisHourShort = 12

        if self.dayNo.endswith('1'):
            self.dayNoAtt = 'st'
        elif self.dayNo.endswith('2'):
            self.dayNoAtt = 'nd'
        elif self.dayNo.endswith('3'):
            self.dayNoAtt = 'rd'
        if int(self.dayNo) >= 11 and int(self.dayNo) <= 19:
            self.dayNoAtt = 'th'

        self.dayLong = daysInFull[self.dayShort]
        self.monthLong = monthsInFull[self.monthShort]

        self.hours = range(1, 13)
        self.mins = range(0, 60)

        ctimeS = time.ctime().split(' ')
        daysAccrdToCalendar = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun')
        realDaysAccrd = ['', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        lastMonthDayNo = calendar.monthrange(int(ctimeS[len(ctimeS) - 1]), self.months.index(self.monthShort) + 1)[1]
        thisMonthStart = daysAccrdToCalendar[
            calendar.monthrange(int(ctimeS[len(ctimeS) - 1]), self.months.index(self.monthShort) + 1)[0]]

        firstWeekEndsOn = 8 - realDaysAccrd.index(thisMonthStart)
        lastWeekend = firstWeekEndsOn

        try:
            ctimeS.remove('')
        except:
            pass

        countOfWeeks = 1
        while lastWeekend < int(ctimeS[2]):
            lastWeekend += 7
            countOfWeeks += 1
        self.weekNo = countOfWeeks
        self.weekNoAtt = ['', 'st', 'nd', 'rd', 'th', 'th', 'th'][self.weekNo]

        self.time__ = '%s:%s%s' % (self.thisHourShort, self.thisMin, self.AMPM)
        self.date__ = '%s %s%s %s %s' % (self.dayLong, self.dayNo, self.dayNoAtt, self.monthShort, self.yearNo)

    def calculatePreviousDate(self, currentDate):
        currentDay = currentDate.split(' ')[1]
        currentMonth = currentDate.split(' ')[0]
        currentYear = currentDate.split(' ')[2]

        if int(currentYear) // 4 == 0:
            self.monthsDays['Feb'] = 29

        newDayNumber = int(currentDay) - 1
        newMonthIndex = self.months.index(currentMonth)
        newYear = int(currentYear)

        if newDayNumber < 1:
            newMonthIndex = self.months.index(currentMonth) - 1
            if newMonthIndex < 1:
                newMonthIndex = 12
                newYear = int(currentYear) - 1
            newDayNumber = self.monthsDays[self.months[newMonthIndex]]

        elif newDayNumber > self.monthsDays[currentMonth]:
            newMonthIndex = self.months.index(currentMonth) + 1
            if newMonthIndex > 12:
                newMonthIndex = 1
                newYear = int(currentYear) + 1
            newDayNumber = 1

        newMonth = self.months[newMonthIndex]

        prevDate = '%s %s %s' % (newMonth, newDayNumber, newYear)
        return prevDate

    def calculateNextDate(self, currentDate):
        currentDay = currentDate.split(' ')[1]
        currentMonth = currentDate.split(' ')[0]
        currentYear = currentDate.split(' ')[2]

        if int(currentYear) // 4 == 0:
            self.monthsDays['Feb'] = 29

        newDayNumber = int(currentDay) + 1
        newMonthIndex = self.months.index(currentMonth)
        newYear = int(currentYear)

        if newDayNumber > self.monthsDays[currentMonth]:
            newMonthIndex = self.months.index(currentMonth) + 1
            if newMonthIndex > 12:
                newMonthIndex = 1
                newYear = int(currentYear) + 1
            newDayNumber = 1

        newMonth = self.months[newMonthIndex]

        prevDate = '%s %s %s' % (newMonth, newDayNumber, newYear)
        return prevDate

Time__ = Time()


def fprint(*text):
    logging.info(str(text))


def give_semester_and_session():
    first_semester_months = Time__.months[7:]
    second_semester_months = Time__.months[0:6]

    semester = ''
    session = './.'
    session_start_year, session_end_year = '', ''

    if Time__.monthShort in first_semester_months:
        semester = 'first'
        session_start_year = str(int(Time__.yearNo))
        session_end_year = str(int(Time__.yearNo) + 1)

    elif Time__.monthShort in second_semester_months:
        semester = 'second'
        session_start_year = str(int(Time__.yearNo) - 1)
        session_end_year = str(Time__.yearNo)

    session = '%s/%s' % (session_start_year, session_end_year)

    return (semester, session)

this_semester, this_session = give_semester_and_session()


class Webview(Widget):
    def __init__(self, **kwargs):
        super(Webview, self).__init__(**kwargs)

        address = kwargs['address']

        if platform == 'android':
            global webview
            webview = True

            from jnius import autoclass
            from android.runnable import run_on_ui_thread

            self.WebView = autoclass('android.webkit.WebView')
            self.WebViewClient = autoclass('android.webkit.WebViewClient')
            self.activity = autoclass('org.renpy.android.PythonActivity').mActivity
            app_ = self.activity.currentFocus

            @run_on_ui_thread
            def create_webview(address):
                self.webview = self.WebView(self.activity)
                self.webview.getSettings().setJavaScriptEnabled(True)
                self.webview.getSettings().setUseWideViewPort(True)
                self.webview.getSettings().setLoadWithOverviewMode(True)
                self.webview.getSettings().setSupportZoom(True)
                self.webview.getSettings().setBuiltInZoomControls(True)
                wvc = self.WebViewClient()
                self.webview.setWebViewClient(wvc)
                self.activity.setContentView(self.webview)
                self.webview.loadUrl(address)

            global detach_webview
            @run_on_ui_thread
            def detach_webview():
                self.webview.clearHistory()
                self.webview.clearCache(True)
                self.webview.loadUrl("about:blank")
                self.webview.freeMemory()  # probably not needed anymore
                self.webview.pauseTimers()  # this should stop any playing content like videos etc. in the background; probably not needed because of 'about:blank' above
                self.activity.setContentView(app_)# sets cached view as an active view

                global webview
                webview = False

            Clock.schedule_once(lambda dt: create_webview(address), 0)

        elif platform == 'win':
            import webbrowser
            webbrowser.open(address)


class SubmitAssignmentScreen(Screen):
    def __init__(self, **kwargs):
        super(SubmitAssignmentScreen, self).__init__(**kwargs)

        self.name = 'Submit Assignment Screen'


class ProfileScreen(Screen):
    def __init__(self, **kwargs):
        super(ProfileScreen, self).__init__(**kwargs)

        self.name = 'Profile Screen'

        self.the_box = BoxLayout(orientation='vertical')
        self.add_widget(self.the_box)
        # self.the_box.add_widget(Label())

        global write_profile
        def write_profile():
            return


class StudyTipsScreen(Screen):
    def __init__(self, **kwargs):
        super(StudyTipsScreen, self).__init__(**kwargs)

        self.name = 'Study Tips Screen'

        self.grid = GridLayout(cols=1)
        self.grid.bind(minimum_height=self.grid.setter('height'))

        self.scroll = ScrollView()
        self.add_widget(self.scroll)


        def get_new_study_tips():
            all_study_tips = membank.get('study_tips')['study_tips']
            new_tips = webengine.get_study_tips()
            all_study_tips += new_tips

            membank.put('study_tips', study_tips=all_study_tips)

        threading.Timer(0, get_new_study_tips).start()

        # all_study_tips = membank.get('study_tips')['study_tips']
        all_study_tips = ['15 minutes of study a day']
        all_study_tips.reverse()
        for a_tip in all_study_tips:
            self.grid.add_widget(Button(text=a_tip, background_normal='white', background_down='white', font_size=mid_font,
                                        font_name=light))
            self.grid.size_hint_y = len(self.grid.children) * .4

        all_study_tips.reverse()
        self.scroll.size_hint_y += 0.0000001


class ManageCoursesScreen(Screen):
    def __init__(self, **kwargs):
        super(ManageCoursesScreen, self).__init__(**kwargs)

        self.name = 'Manage Courses Screen'

        self.the_box = BoxLayout(orientation='vertical')
        self.add_widget(self.the_box)

        self.cols = ('code', 'title', 'units')
        self.selected_courses = []

        s_details = membank.get('student_details')['student_details']
        fullname = s_details['fullname']
        dept = s_details['department']
        level = s_details['level']

        def show_new_courses(button):
            the_grid = GridLayout(cols=4)
            the_grid.bind(minimum_height=the_grid.setter('height'))

            the_scroll = ScrollView(do_scroll_x=False)
            the_scroll.add_widget(the_grid)

            def add_courses(popup):
                sem_session = '%s %s' % (this_semester, this_session)
                fprint('Sent cf: %s'%self.selected_courses)
                threading.Timer(0, lambda:webengine.register_courses(s_details['matric_no'], self.selected_courses,
                                                                     sem_session)).start()

                this_sem = '%s %s' % (this_semester, this_session)

                stu_details = membank.get('student_details')['student_details']
                all_course_forms = stu_details['course_history']
                all_course_forms[this_sem] = self.selected_courses
                stu_details['course_history'] = all_course_forms

                Clock.schedule_once(lambda dt: membank.put('student_details', student_details=stu_details), 1)

                show_this_cf()

            course_selection_p = PeoplePopup(title='', separator_color=(0,0,0,0), size_hint=(.8, .8),
                                             content=the_scroll)
            course_selection_p.bind(on_dismiss=add_courses)
            course_selection_p.open()

            def get_and_write_courses():
                sem_session = '%s %s' % (this_semester, this_session)
                course_tree = webengine.get_course_tree(membank.get('student_details')['student_details']['matric_no'],
                                                        sem_session)

                self.selected_courses = []
                fprint('Course tree: %s' % course_tree)

                def do_select_course(cb, touch):
                    if cb.collide_point(touch.x, touch.y):
                        this_index = the_grid.children.index(cb)

                        codelabel_index = this_index + 3
                        codelabel = the_grid.children[codelabel_index]

                        descriptionlabel_index = this_index + 2
                        descriptionlabel = the_grid.children[descriptionlabel_index]

                        unitslabel_index = this_index + 1
                        unitslabel = the_grid.children[unitslabel_index]

                        dict_ = {'code': str(codelabel.text), 'title': str(descriptionlabel.text),
                                 'units': str(unitslabel.text)}

                        if 'registered' in dict_:
                            dict_.pop('registered')

                        if cb.active:
                            if dict_ not in self.selected_courses:
                                self.selected_courses.append(dict_)
                        else:
                            try:
                                # dict_['status'] = 'R'
                                self.selected_courses.remove(dict_)
                            except:
                                try:
                                    # dict_['status'] = 'R'
                                    self.selected_courses.remove(dict_)
                                except:
                                    pass

                        fprint(self.selected_courses)

                def write_course_tree(dt):
                    # print('downloaded course_tree: %s'%course_tree)

                    size_x = .25 * Window.width
                    if not mobile:
                        size_x = .6 * Window.width

                    for a_course in course_tree:
                        for col in self.cols:
                            this_label = Label(text=a_course[col], color=(0,0,0,1), size_hint_x=.2)
                            the_grid.add_widget(this_label)

                            if col == 'title':
                                this_label.text_size = (size_x, None)
                                this_label.halign = 'center'
                                this_label.size_hint_x = .5

                        checkbox_ = CheckBox(size_hint_x=.2)
                        checkbox_.bind(on_touch_up=do_select_course)
                        the_grid.add_widget(checkbox_)

                        if 'registered' in a_course and a_course['registered']:
                            a_course.pop('registered')
                            checkbox_.active = True
                            self.selected_courses.append(a_course)

                    the_grid.size_hint_y = (len(the_grid.children) // (len(self.cols) + 1)) * .15
                    the_scroll.size_hint_y += 0.000001

                Clock.schedule_once(write_course_tree, 1)

            threading.Timer(0, get_and_write_courses).start()

        this_semester_bar_box = BoxLayout(size_hint_y=.1)

        this_semester_bar = Button(text='%s Semester, %s.' % (this_semester.upper(), this_session),
                                   background_color=get_color_from_hex(d_blue), markup=True,
                                   font_name=light, font_size=big_font)
        add_new_course_button = Button(text='+', size_hint_x=None, width=cm(2.5), font_name=light, font_size=bigger_font,
                                       on_release=show_new_courses, background_normal='white',
                                       background_color=(1,1,1,1), color=(0,0,0,.6))
        this_semester_bar_box.add_widget(this_semester_bar)
        this_semester_bar_box.add_widget(add_new_course_button)

        self.the_box.add_widget(this_semester_bar_box)

        self.the_grid = GridLayout(cols=len(self.cols), spacing=5)
        self.the_grid.bind(minimum_height=self.the_grid.setter('height'))

        self.the_scroll = ScrollView(do_scroll_x=False, size_hint_y=.7)
        self.the_scroll.add_widget(self.the_grid)

        def show_this_cf():
            self.the_grid.clear_widgets()
            self.the_grid.size_hint_y = 0
            self.the_scroll.size_hint_y += 0.000001

            for col_ in self.cols:
                this_button = Button(text=col_.title(), font_name=light, font_size=big_font, background_normal='white',
                                     background_color=get_color_from_hex(blue))
                if col_ == 'title':
                    this_button.size_hint_x = 1.5
                self.the_grid.add_widget(this_button)

            this_sem = '%s %s' % (this_semester, this_session)

            all_course_forms = membank.get('student_details')['student_details']['course_history']

            this_semester_cf = []
            if this_sem in all_course_forms:
                this_semester_cf = all_course_forms[this_sem]

            total_unit_count = 0

            for course_ in this_semester_cf:
                for col_ in self.cols:
                    self.the_grid.add_widget(Label(text=course_[col_], font_name=bold, font_size=mid_font,
                                                   color=(.2, .2, .2, 1)))

                total_unit_count += int(course_['units'])

                units_count.text = str(total_unit_count)

            def next_col(textinput):
                textinput.focus = False
                if self.the_grid.children.index(self) > 0:
                    self.the_grid.chidren[self.the_grid.children.index(textinput) - 1].focus = True

            self.the_grid.size_hint_y = (len(self.the_grid.children) / len(self.cols)) * .12
            self.the_scroll.size_hint_y += 0.000001

        self.the_box.add_widget(self.the_scroll)

        status_bar = BoxLayout(size_hint_y=.2)

        units_count = Button(text='', font_name=bold, font_size=biggest_font, background_normal='white',
                             background_color=(1,1,1,1), color=(0,0,0,.8))
        status_bar.add_widget(units_count)

        self.the_box.add_widget(status_bar)

        show_this_cf()


class ResultScreen(Screen):
    def __init__(self, **kwargs):
        super(ResultScreen, self).__init__(**kwargs)

        self.name = 'Result Screen'

        self.box = BoxLayout(orientation='vertical')
        self.add_widget(self.box)


class NotepadScreen(Screen):
    def __init__(self, **kwargs):
        super(NotepadScreen, self).__init__(**kwargs)

        self.name = 'Notepad Screen'

        title_bar = BoxLayout(orientation='horizontal')
        title_bar.add_widget(Button(text='{course_title}\n{student_name}', size_hint_y=.09))
        title_bar.add_widget(Button(text='Save', size_hint_x=.1))

        title_input = TextInput(hint_text='Title', size_hint_y=.09)
        body_input = TextInput(hint_text='Content')

        body_input_grid = GridLayout(cols=1, size_hint_y=2)
        body_input_grid.add_widget(body_input)
        body_input_grid.bind(minimum_height=body_input_grid.setter('height'))
        body_input_scroll = ScrollView(do_scroll_x=False)
        body_input_scroll.add_widget(body_input_grid)

        self.box = BoxLayout(orientation='vertical')
        self.box.add_widget(title_bar)
        self.box.add_widget(title_input)
        self.box.add_widget(body_input_scroll)
        self.add_widget(self.box)


class PaymentScreen(Screen):
    def __init__(self, **kwargs):
        super(PaymentScreen, self).__init__(**kwargs)

        self.name = 'Payment Screen'


class AboutScreen(Screen):
    def __init__(self, **kwargs):
        super(AboutScreen, self).__init__(**kwargs)

        self.name = 'About Screen'


class SplashScreen(Screen):
    def __init__(self, **kwargs):
        super(SplashScreen, self).__init__(**kwargs)

        self.name = 'Splash Screen'

        self.add_widget(Label(text='[color=404040]%s[size=%s]\n%s[/size][/color]' % (__title__, mid_font, __author__),
                              font_name=college, font_size=font_scale(75), markup=True, halign='center',
                              text_size=(.7*Window.width, None)))

        def load_home_screen(dt):
            screenman_.add_widget(HomeScreen())
            screenman_.current = 'Home Screen'

        Clock.schedule_once(load_home_screen, 10)


class HomeScreen(Screen):
    def __init__(self, **kwargs):
        super(HomeScreen, self).__init__(**kwargs)

        self.name = 'Home Screen'
        global home_screen
        home_screen = self

        self.box = BoxLayout(orientation='vertical')
        self.add_widget(self.box)

        portal = ['Home', 'Profile', 'Manage Courses', 'Payment', 'Result']

        def sync_data():
            'get all login data again'
            pass

        class ProductivityPane(FloatLayout):
            def __init__(self, **kwargs):
                super(ProductivityPane, self).__init__(**kwargs)

                self.add_widget(Button(background_normal='white', background_color=(.35, .35, .35, 1),
                                       pos_hint={'center_x':.5, 'center_y':.5}))

                self.scroll = ScrollView(do_scroll_x=False, pos_hint={'center_x':.5, 'center_y':.5})
                self.add_widget(self.scroll)

                self.grid = GridLayout(cols=1, spacing=3)
                self.grid.bind(minimum_height=self.grid.setter('height'))
                self.scroll.add_widget(self.grid)

                self.options = ['Timetable', 'Study Tips', 'Logout']
                screen_name = {'Home': HomeScreen, 'Profile': ProfileScreen, 'Study Tips': StudyTipsScreen,
                               'Manage Courses': ManageCoursesScreen, 'Payment': PaymentScreen, 'Result': ResultScreen,
                               'About': AboutScreen}

                def show_pane(button):
                    for heading_button in self.grid.children:
                        heading_button.background_color = (.75, .75, .75, .2)
                        heading_button.background_color[3] -= .7

                    button.background_color = get_color_from_hex(blue)
                    button.background_color[3] -= .7

                    if button.text in ['Payment', 'Room Allocation', 'Result']:
                        Webview(address='http://portal.abuad.edu.ng')

                    if '%s Screen' % button.text in screenman_.screens:
                        screenman_.current = '%s Screen' % button.text
                    else:
                        if '%s Screen' % button.text not in screenman_.screens:
                            screenman_.add_widget(screen_name[button.text]())
                        screenman_.current = '%s Screen' % button.text

                    if mobile:
                        productivity_popup.dismiss()

                text_size_x = .15*Window.width
                if mobile:
                    text_size_x = .75*Window.width

                for opt in portal:
                    self.grid.add_widget(Button(text=opt, background_normal=''.join(opt.split(' ')), font_name=light,
                                                font_size=mid_font, background_color=(.75, .75, .75, 0),
                                                text_size=(text_size_x, None), color=(.9,.9,.9,1),
                                                on_release=show_pane))

                self.grid.children[-1].background_color = get_color_from_hex(blue)
                self.grid.children[-1].background_color[3] -= .7

                self.grid.size_hint_y = len(self.grid.children) * .09
                self.scroll.size_hint_y += 0.000001

        class AlertsTab(ScrollView):
            def __init__(self, **kwargs):
                super(AlertsTab, self).__init__(**kwargs)

                self.do_scroll_x = False

                order = ['Reminders', 'Announcements', 'Social Events']

                class AssignmentsCard(BoxLayout):
                    def __init__(self, **kwargs):
                        super(AssignmentsCard, self).__init__(**kwargs)

                        assignmentscard = self

                        self.orientation = 'vertical'

                        self.grid = GridLayout(cols=1, spacing=1)
                        self.grid.bind(minimum_height=self.grid.setter('height'))

                        self.scroll = ScrollView(do_scroll_x=False, size_hint_y=.75, bar_color=get_color_from_hex(red),
                                                 bar_width=cm(.5))
                        self.scroll.add_widget(self.grid)

                        self.card_heading = Button(text='Assignments', background_normal='white',
                                                   background_down='white', color=get_color_from_hex(red),
                                                   font_size=bigger_font, size_hint_y=.25, font_name=bold,
                                                   background_color=(1,1,1,1))
                        self.add_widget(self.card_heading)
                        self.add_widget(self.scroll)

                        try:
                            saved_assignments = membank.get('saved_assignments')['saved_assignments']
                        except:
                            saved_assignments = None

                        threading.Timer(1.5, lambda: self.refresh(saved_assignments)).start()

                    def get_new_assignments(self):
                        all_course_forms = membank.get('student_details')['student_details']['course_history']
                        try:
                            this_semester_cf = all_course_forms['%s %s' % (this_semester, this_session)]
                        except:
                            this_semester_cf = []

                        all_assignments = []

                        for course_ in this_semester_cf:
                            course_assignments = webengine.get_assignment(course_['code'], '%s %s' % (
                                this_semester, this_session))
                            all_assignments.extend(course_assignments)

                        Clock.schedule_once(lambda dt: membank.put('saved_assignments',
                                                                   saved_assignments=all_assignments), 0)

                    def show_assignment(self, button):
                        fprint('')

                    def show_assignments_notifications(self):
                        saved_assignments = membank.get('saved_assignments')['saved_assignments']

                        if len(saved_assignments) == 0:
                            notification.notify('You have no assignments.', 'Woo-hoo!', {})

                        else:
                            for assignment in saved_assignments:
                                title = assignment['question']
                                deadline = assignment['deadline']
                                lecturer = assignment['lecturer']

                                notification.notify('Assignment (%s).' % title,
                                                    '%s\'s assignment is due on %s' % (lecturer, deadline), {})

                    def write_saved_assignments(self):
                        saved_assignments = membank.get('saved_assignments')['saved_assignments']

                        if len(self.grid.children) > 0 and type(self.grid.children[0]) is Label:
                            self.grid.clear_widgets()

                        for assignment in saved_assignments:
                            title = assignment['question']
                            deadline = assignment['deadline']
                            lecturer = assignment['lecturer']

                            text_ = '[font=%s]%s[/font][size=%s]\n%s (%s)[/size]' % (bold, title, mid_font, deadline,
                                                                                     lecturer)

                            self.grid.add_widget(Button(
                                text=text_, background_normal='white', color=(0, 0, 0, 1), font_name=light,
                                font_size=bigger_font, markup=True, text_size=(.95 * self.size[0], None),
                                on_release=self.show_assignment, background_color=(1,1,1,.7)))

                        if len(saved_assignments) > 0:
                            self.card_heading.text += ' (%s)' % str(len(saved_assignments))
                            self.grid.size_hint_y = len(saved_assignments) * .35
                            self.scroll.size_hint_y += 0.000001

                        else:
                            text_x = .4 * home_screen.the_tabs_holder.width
                            if mobile:
                                text_x = .7 * home_screen.the_tabs_holder.width

                            self.grid.add_widget(Label(text='Whoop! You don\'t have any assignment. (for now!)',
                                                       text_size=(text_x, None),
                                                       font_name=light,
                                                       font_size=bigger_font, halign='center', color=(0, 0, 0, .5)))
                            self.grid.add_widget(Widget())
                            self.grid.size_hint_y = 1.2
                            self.scroll.size_hint_y += 0.0000001

                        self.get_new_assignments()

                    def refresh(self, assignments_list):
                        if not assignments_list:
                            self.get_new_assignments()

                        self.write_saved_assignments()
                        self.show_assignments_notifications()
                        # Clock.schedule_once(lambda dt: self.write_saved_assignments(), 2)

                class AnnouncementCard(BoxLayout):
                    def __init__(self, **kwargs):
                        super(AnnouncementCard, self).__init__(**kwargs)

                        self.orientation = 'vertical'

                        self.grid = GridLayout(cols=1, spacing=1)
                        self.grid.bind(minimum_height=self.grid.setter('height'))

                        self.scroll = ScrollView(do_scroll_x=False, size_hint_y=.75, bar_color=get_color_from_hex(red),
                                                 bar_width=cm(.5))
                        self.scroll.add_widget(self.grid)

                        self.card_heading = Button(text='Announcements', background_normal='white',
                                                   background_down='white', color=get_color_from_hex(red),
                                                   font_size=bigger_font, size_hint_y=.25, font_name=bold,
                                                   background_color=(1,1,1,1))
                        self.add_widget(self.card_heading)
                        self.add_widget(self.scroll)

                        try:
                            saved_announcements = membank.get('saved_announcements')['saved_announcements']
                        except:
                            saved_announcements = None

                        threading.Timer(1, lambda: self.refresh(saved_announcements)).start()

                    def get_new_announcements(self):
                        announcements = webengine.get_announcements()
                        membank.put('saved_announcements', saved_announcements=announcements)

                    def show_announcements(self, button):
                        announcement = eval(button.background_normal)

                        grid = GridLayout(cols=1)
                        grid.add_widget(Label(text='%s[size=%s]\n%s[/size]' % (announcement['author'], big_font,
                                                                               announcement['time']),
                                              font_name=bold, font_size=giant_font, markup=True, halign='center',
                                              color=(.1, .1, .1, .9), text_align='center', size_hint_y=.2))
                        grid.add_widget(TextInput(text=announcement['message'], font_name=light, font_size=mid_font,
                                                  readonly=True, color=(0, 0, 0, 1), background_color=(0, 0, 0, 0)))
                        # grid.bind(minimum_height=grid.setter('height'))

                        # scroll = ScrollView(do_scroll_x=False)
                        # scroll.add_widget(grid)

                        PeoplePopup(separator_color=(0, 0, 0, 0), content=grid, size_hint=(.8, .7)).open()

                    def write_stored_announcements(self):
                        try:
                            saved_announcements = membank.get('saved_announcements')['saved_announcements']
                        except:
                            saved_announcements = []
                            membank.put('saved_announcements', saved_announcements=[])

                        for announcement in saved_announcements:
                            message = announcement['message']
                            author = announcement['author']
                            time = announcement['time']
                            venue = announcement['venue']

                            message_short = message[:32]
                            if len(message) > 32:
                                message_short += '...'

                            text_ = '[color=101010][font=%s]%s[/font][size=%s][/color]\n[color=606060]%s @%s \n%s[/color][/size]' % (bold, message_short, mid_font,
                                                                                         time, venue, author)

                            self.grid.add_widget(Button(
                                text=text_, background_normal=str(announcement), font_name=light,
                                font_size=bigger_font, markup=True, text_size=(.95 * self.size[0], None),
                                on_release=self.show_announcements, background_color=(1,1,1,.7)))

                        self.card_heading.text += ' (%s)' % str(len(saved_announcements))
                        self.grid.size_hint_y = len(saved_announcements) * .575
                        self.scroll.size_hint_y += 0.000001

                        self.get_new_announcements()

                    def refresh(self, announcements_list):
                        if not announcements_list:
                            self.get_new_announcements()

                        self.write_stored_announcements()
                        # Clock.schedule_once(lambda dt: self.write_stored_announcements(), 1.4)

                class StudyTipsCard(BoxLayout):
                    def __init__(self, **kwargs):
                        super(StudyTipsCard, self).__init__(**kwargs)

                        self.orientation = 'vertical'

                        self.grid = GridLayout(cols=1, spacing=1)
                        self.grid.bind(minimum_height=self.grid.setter('height'))

                        self.scroll = ScrollView(do_scroll_x=False, size_hint_y=.75, bar_color=get_color_from_hex(red),
                                                 bar_width=cm(.5))
                        self.scroll.add_widget(self.grid)

                        self.card_heading = Button(text='Study Tips', background_normal='white', background_down='white',
                                               color=get_color_from_hex(red), font_size=bigger_font,
                                               size_hint_y=.3, font_name=bold, background_color=(1,1,1,1))
                        self.add_widget(self.card_heading)
                        self.add_widget(self.scroll)
                        try:
                            saved_study_tips = membank.get('saved_study_tips')['saved_study_tips']
                        except:
                            saved_study_tips = None

                        threading.Timer(2, lambda: self.refresh(saved_study_tips)).start()

                    def get_new_study_tips(self):
                        study_tips = webengine.get_study_tips()

                        try:
                            old_tips = membank.get('saved_study_tips')['saved_study_tips']
                        except KeyError:
                            old_tips = []

                        if type(old_tips) is not list:
                            old_tips = []

                        old_tips.append(study_tips)
                        membank.put('saved_study_tips', saved_study_tips=old_tips, time=time.time())

                    def show_study_tip(self, button):
                        study_tip = eval(button.background_normal)

                        grid = GridLayout(cols=1)
                        grid.add_widget(Label(text=study_tip['title'], font_name=bold, font_size=giant_font,
                                              color=(.1, .1, .1, .9), size_hint_y=.2))
                        grid.add_widget(TextInput(text=study_tip['body'], font_name=light, font_size=mid_font,
                                                  readonly=True, color=(0, 0, 0, 1), background_color=(0, 0, 0, 0)))
                        # grid.bind(minimum_height=grid.setter('height'))

                        # scroll = ScrollView(do_scroll_x=False)
                        # scroll.add_widget(grid)

                        PeoplePopup(separator_color=(0, 0, 0, 0), content=grid, size_hint=(.8, .7)).open()

                    def write_stored_study_tips(self):
                        try:
                            saved_study_tips = membank.get('saved_study_tips')['saved_study_tips']
                        except:
                            saved_study_tips = []
                            membank.put('saved_study_tips', saved_study_tips=[], time=time.time())

                        for study_tip in saved_study_tips:
                            title = study_tip['title']
                            body = study_tip['body']

                            body_short = body[:60]
                            if len(body) > 60:
                                body_short += '...'

                            text_ = '[font=%s]%s[/font][size=%s]\n%s[/size]' % (bold, title, mid_font, body_short)

                            self.grid.add_widget(Button(
                                text=text_, background_normal=str(study_tip), color=(0, 0, 0, 1), font_name=light,
                                font_size=bigger_font, markup=True, text_size=(.95 * self.size[0], None),
                                on_release=self.show_study_tip, background_color=(1,1,1,.7)))

                        self.card_heading.text += ' (%s)' % str(len(saved_study_tips))
                        self.grid.size_hint_y = len(saved_study_tips) * .47
                        self.scroll.size_hint_y += 0.000001

                        if (time.time() - membank.get('saved_study_tips')['time']) > 40000:  # second arg is to make sure it is after a day
                            threading.Timer(0, self.get_new_study_tips).start()

                    def refresh(self, study_tips):
                        if not study_tips:
                            self.get_new_study_tips()

                        self.write_stored_study_tips()
                        # Clock.schedule_once(lambda dt: self.write_stored_study_tips(), 1.1)

                self.grid = GridLayout(cols=1, size_hint_y=1.5, spacing=20, padding=(20, 20, 20, 0))
                self.grid.bind(minimum_height=self.grid.setter('height'))
                self.add_widget(self.grid)

                if not mobile:
                    self.grid.cols = 2
                    self.grid.size_hint_y = 1

                self.grid.add_widget(AssignmentsCard())
                self.grid.add_widget(AnnouncementCard())
                self.grid.add_widget(StudyTipsCard())

                self.size_hint_y += 0.00001

                self.card_scroll_in_progress = False

            def on_touch_down(self, touch):
                for card in self.grid.children:
                    card_scroll = card.scroll

                    if card_scroll.collide_point(touch.x, touch.y):
                        self.card_scroll_in_progress = True

                        if card.card_heading.collide_point(touch.x, touch.y):
                            self.card_scroll_in_progress = False

                        break

                ScrollView.on_touch_down(self, touch)

            def on_touch_move(self, touch):
                if self.card_scroll_in_progress:
                    self.scroll_timeout = 0
                else:
                    self.scroll_timeout = 5000

                ScrollView.on_touch_move(self, touch)

            def on_touch_up(self, touch):
                self.card_scroll_in_progress = False
                ScrollView.on_touch_up(self, touch)

        class RadarCard(FloatLayout):
            def __init__(self, **kwargs):
                super(RadarCard, self).__init__(**kwargs)
                
                self.dict_ = dict_ = kwargs['dict_']
                grid = kwargs['grid']

                self.add_widget(AsyncImage(source=dict_['cover'], allow_stretch=True, keep_ratio=False,
                                           pos_hint={'center_x': .5, 'center_y': .5}, color=(.35, .35, .35, 1)))
                # self.add_widget(Label(font_name=light, font_size=big_font, pos_hint={'center_x': .5, 'center_y': .5},
                #                       text_size=(.9*self.size[0], None), color=(0,0,0,1)))

                def do_like(button):
                    threading.Timer(0, lambda:webengine.like_radar_post(
                        membank.get('student_details')['student_details']['matric_no'], self.dict_['id'])).start()

                    self.dict_['likes'] += 1
                    self.like_count.text = str(self.dict_['likes'])
                    self.like_button.source = 'love_icon_filled.png'
                    self.like_button.color = get_color_from_hex(red)
                    
                color_ = (1, 1, 1, 1)
                source_ = 'love_icon.png'
                if self.dict_['liked']:
                    color_ = get_color_from_hex(red)
                    source_ = 'love_icon_filled.png'
                
                self.like_button = AsyncImage(source=source_, allow_stretch=True, keep_ratio=True,
                                              pos_hint={'center_x': .1, 'center_y': .15}, action=do_like, color=color_,
                                              size_hint=(.2, .2), mipmap=False)
                self.like_count = Label(text=str(self.dict_['likes']), font_name=light, font_size=mid_font,
                                        pos_hint={'center_x': .175, 'center_y': .15}, color=color_)
                self.add_widget(self.like_button)
                self.add_widget(self.like_count)

                self.front_box = BoxLayout(orientation='vertical', pos_hint={'center_x': .5, 'center_y': .6},
                                           size_hint_y=.7)
                self.add_widget(self.front_box)

                self.description_label = Label(text='[font=%s][size=%s]%s[/size][/font]\n[font=%s]%s[size=%s]\n\n%s[/font]\n%s | %s, %sL[/size]' % (
                    college, font_scale(60), dict_['title'], bold, dict_['description'], mid_font, dict_['created_at'],
                    dict_['creator']['fullname'], dict_['creator']['department'], dict_['creator']['level']),
                                               markup=True, font_name=light, font_size=bigger_font,
                                               text_size=((.9 * home_screen.the_tabs_holder.width) / grid.cols, None),
                                               halign='center')
                self.front_box.add_widget(self.description_label)

                self.call_to_action_button = Button(text='VIEW', font_name=light, font_size=mid_font,
                                                    background_normal='white',
                                                    background_color=get_color_from_hex(d_gold),
                                                    size_hint=(.3, .3), pos_hint={'center_x': .5, 'center_y': .5},
                                                    on_release=self.view_radar)
                self.front_box.add_widget(self.call_to_action_button)

            def view_radar(self, button):
                Webview(address='%s/radar?id=%s&request=view&matric_no=%s' % (
                    webengine.servers_ip, self.dict_['id'],
                    membank.get('student_details')['student_details']['matric_no']))

        class RadarTab(ScrollView):
            def __init__(self, **kwargs):
                super(RadarTab, self).__init__(**kwargs)

                self.do_scroll_x = False
                self.refreshed = False

                self.grid = GridLayout(cols=2, spacing=10)
                self.grid.bind(minimum_height=self.grid.setter('height'))
                self.add_widget(self.grid)

                if mobile:
                    self.grid.cols = 1

            def get_new_radar(self):
                new_radar = webengine.get_radar(membank.get('student_details')['student_details']['matric_no'])
                fprint('Just got_new_radar: %s' % new_radar)
                Clock.schedule_once(lambda dt:membank.put('radar', radar=new_radar), 0)
                Clock.schedule_once(lambda dt:self.write_stored_radar(), 1)

            def show_radar_adder(self, button):
                Webview(address='%s/radar?request=add_page&matric_no=%s' % (
                    webengine.servers_ip, membank.get('student_details')['student_details']['matric_no']))

            def write_stored_radar(self):
                self.grid.clear_widgets()
                self.grid.add_widget(Button(text='Get your brand, event, product, discovered within ABUAD',
                                            font_name=light, background_normal='white',
                                            background_color=get_color_from_hex(blue), halign='center',
                                            font_size=biggest_font, on_release=self.show_radar_adder,
                                            text_size=(.65*home_screen.the_tabs_holder.width, None)))

                stored_radar = membank.get('radar')['radar']
                fprint('From stored_radar: %s' % stored_radar)

                for post in stored_radar:
                    self.grid.add_widget(RadarCard(dict_=post, grid=self.grid))

                if self.grid.cols == 1:
                    self.grid.size_hint_y = len(self.grid.children) * .65
                else:
                    grid_children = len(self.grid.children) // 2
                    self.grid.size_hint_y = grid_children * .65

                    if len(self.grid.children) % 2 != 0:
                        self.grid.size_hint_y += .65

                self.size_hint_y += 0.00001

                if not self.refreshed:
                    threading.Timer(0, self.get_new_radar).start()
                    self.refreshed = True

            def load_tab(self):
                self.grid.clear_widgets()
                self.write_stored_radar()

        class DayButton(Button):
            def __init__(self, **kwargs):
                super(DayButton, self).__init__(**kwargs)

                day = kwargs['dayNo']
                week = kwargs['week']
                month = kwargs['month']
                year = kwargs['year']
                matric_no = kwargs['matric_no']

                self.background_normal = 'white'
                self.text = str(day)
                self.background_color = (0, 0, 0, 0)
                self.font_name = light
                self.color = (0, 0, 0, .6)
                self.font_size = mid_font

                if str(day) == str(Time__.dayNo) and month == Time__.monthShort and year == Time__.yearNo:
                    self.backgroud_color = (1,1,1,1)
                    self.color = get_color_from_hex(d_gold)
                    self.font_size = big_font

                    week_indicator_box.children[-week].background_color = get_color_from_hex(d_gold)

            def on_press(self):
                return

        class Calendar(BoxLayout):
            def __init__(self, **kwargs):
                super(Calendar, self).__init__(**kwargs)

                global week_indicator_box
                week_indicator_box = BoxLayout(orientation='vertical', size_hint_x=.02)

                for a in range(6):
                    week_indicator_box.add_widget(Button(background_color=(0,0,0,0), background_normal='white',
                                                         background_down='white'))

                self.grid = GridLayout(cols=7)

                self.add_widget(week_indicator_box)
                self.add_widget(self.grid)

                self.year = Time__.yearNo
                self.month = Time__.monthShort

                first_day_of_month = calendar.weekday(int(self.year), Time__.months.index(self.month) + 1, 1) + 1
                if first_day_of_month == 7:
                    first_day_of_month = 0

                for days in ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'):
                    self.grid.add_widget(Label(text=days, color=(0, 0, 0, .5), font_name=bold, font_size=mid_font))

                cnt_ = 0
                while cnt_ < first_day_of_month:
                    self.grid.add_widget(Label())
                    cnt_ += 1

                for a in range(Time__.monthsDays[self.month]):
                    week = (len(self.grid.children) // 7) + 1
                    self.grid.add_widget(DayButton(dayNo=a + 1, month=self.month, year=self.year, week=week,
                                              matric_no=membank.get('student_details')['student_details']['matric_no']))

        calendar_wid = Calendar()

        class ActivityStrip(BoxLayout):
            def __init__(self, **kwargs):
                super(ActivityStrip, self).__init__(**kwargs)

                activitystrip = self

                activity_dict = kwargs['activity']
                print('Activity dict: %s' % activity_dict)
                dayname = kwargs['dayname']

                time = activity_dict['time']
                hour = time.split(':')[0]
                min = time.split(':')[1]

                self.text = ''

                hour_str = str(hour)
                if len(str(hour)) == 1:
                    hour_str = '0%s' % str(hour)

                def show_color(self):
                    fprint(self.background_color)

                def remove_course(self):
                    planner_course_count_dict = membank.get('planner_course_count')['planner_course_count']
                    planner_course_count_dict[activity_dict['course']['code']] -= 1
                    membank.put('planner_course_count', planner_course_count=planner_course_count_dict)

                    try:
                        all_days_data = membank.get('planner')['planner']
                    except:
                        all_days_data = {}
                        membank.put('planner', planner={})

                    try:
                        days_data = all_days_data[dayname]
                    except:
                        days_data = []

                    days_data.remove(activity_dict)
                    all_days_data[dayname] = days_data
                    membank.put('planner', planner=all_days_data)

                    self.parent.remove_widget(activitystrip)

                planner_course_count = membank.get('planner_course_count')['planner_course_count']
                count_ = str(planner_course_count[activity_dict['course']['code']])

                self.add_widget(Button(text='%s[size=%s]%s[/size]' % (hour_str, mid_font, min), font_name=light,
                                       background_normal='white', background_color=(0.28, 0.34, 0.368, 0.9),
                                       font_size=biggest_font, size_hint_x=.25, markup=True, on_press=show_color))
                self.add_widget(Button(text='%s[size=%s][color=808080]\n%s units | %s time(s) a week.[/color][/size]' % (
                                            activity_dict['course']['code'], smid_font, activity_dict['course']['units'],
                                            count_),
                                       color=(.1, .1, .1, .8), font_name=light, font_size=big_font, halign='left',
                                       background_normal='white', markup=True,
                                       text_size=(.67*home_screen.the_tabs_holder.width, None)))
                self.add_widget(Button(text='REMOVE', font_name=light, font_size=small_font, background_normal='white',
                                       color=(0, 0, 0, .5), size_hint_x=.2, on_press=remove_course))

        class DayView(ScrollView):
            def __init__(self, **kwargs):
                super(DayView, self).__init__(**kwargs)

                self.dayname = kwargs['dayname']
                self.refresh()

            def refresh(self):
                dayname = self.dayname
                dayview = self

                self.clear_widgets()

                # membank.put('planner', planner={})

                try:
                    all_days_data = membank.get('planner')['planner']
                except:
                    all_days_data = {}
                    membank.put('planner', planner={})

                try:
                    days_data = all_days_data[dayname]
                except:
                    days_data = []

                self.do_scroll_x = False

                self.grid = GridLayout(cols=1, spacing=2)
                self.add_widget(self.grid)

                for activity in days_data:
                # for hour in range(0, 24):
                    self.grid.add_widget(ActivityStrip(dayname=dayname, activity=activity))

                def do_add_course_to_planner(self):
                    all_courses = membank.get('student_details')['student_details'][
                        'course_history']['%s %s' % (this_semester, this_session)]

                    course_code = course_select.text

                    for dict_ in all_courses:
                        if dict_['code'] == course_code:
                            break

                    try:
                        planner_course_count = membank.get('planner_course_count')['planner_course_count']
                    except:
                        planner_course_count = {}

                    try:
                        planner_course_count[dict_['code']] += 1
                    except:
                        planner_course_count[dict_['code']] = 1

                    membank.put('planner_course_count', planner_course_count=planner_course_count)

                    days_data.append({'course': {'code': dict_['code'], 'units': dict_['units']},
                                      'time': '%s:%s%s' % (hour_select.text, min_select.text, am_pm_select.text)})
                    all_days_data[dayname] = days_data
                    membank.put('planner', planner=all_days_data)

                    add_planner_popup.dismiss()
                    dayview.refresh()

                def show_study_adder(self):
                    add_box = BoxLayout(orientation='vertical')

                    try:
                        all_courses = membank.get('student_details')['student_details'][
                            'course_history']['%s %s'%(this_semester, this_session)]
                    except:
                        all_courses = []

                    all_course_codes = []

                    for dict_ in all_courses:
                        all_course_codes.append(dict_['code'])

                    if all_course_codes == []:
                        toast('You have to register courses for this semester, to begin.')
                        return

                    global course_select

                    course_select_bar = BoxLayout()
                    course_select = Spinner(text=all_course_codes[0], values=all_course_codes, color=(0, 0, 0, 1),
                                            background_color=(0, 0, 0, .3),
                                            background_normal='atlas://trail/pinkBar', font_name='Roboto-Regular.ttf',
                                            font_size=big_font)
                    course_select_bar.add_widget(course_select)
                    course_select_bar.add_widget(Widget())

                    time_select_bar = BoxLayout()
                    hours = [str(y) for y in Time__.hours]
                    mins = [str(x) for x in range(0, 60, 5)]

                    mins[0], mins[1] = '00', '05'

                    global hour_select, min_select, am_pm_select
                    hour_select = Spinner(text=str(Time__.thisHourShort), values=hours, color=(0, 0, 0, 1),
                                            background_color=(0, 0, 0, 0),
                                            background_normal='atlas://trail/pinkBar', font_name='Roboto-Regular.ttf',
                                            font_size=big_font)
                    min_select = Spinner(text=Time__.thisMin, values=mins, color=(0, 0, 0, 1),
                                         background_color=(0, 0, 0, 0), background_normal='atlas://trail/pinkBar',
                                         font_name='Roboto-Regular.ttf', font_size=big_font)
                    am_pm_select = Spinner(text=Time__.AMPM, values=['am', 'pm'], color=(0, 0, 0, 1),
                                         background_color=(0, 0, 0, 0), background_normal='atlas://trail/pinkBar',
                                         font_name='Roboto-Regular.ttf', font_size=big_font)
                    time_select_bar.add_widget(hour_select)
                    time_select_bar.add_widget(min_select)
                    time_select_bar.add_widget(am_pm_select)
                    time_select_bar.add_widget(Widget())

                    add_box.add_widget(course_select_bar)
                    add_box.add_widget(time_select_bar)
                    add_box.add_widget(Button(text='Add', on_release=do_add_course_to_planner))

                    global add_planner_popup
                    add_planner_popup = PeoplePopup(title='Add a course to study on %s' % self.background_normal,
                                              separator_color=(0,0,0,0), content=add_box, size_hint=(.75, .5))
                    add_planner_popup.open()

                self.grid.add_widget(Button(text='+ What do you wish to study on %ss?' % (dayname), font_name=light,
                                            font_size=big_font, background_normal=dayname,
                                            background_color=(.75, .75, .75, 1), color=get_color_from_hex(d_blue),
                                            on_release=show_study_adder))

                self.grid.size_hint_y = len(self.grid.children) * .28
                self.size_hint_y += 0.00001

        class PlannerTab(BoxLayout):
            def __init__(self, **kwargs):
                super(PlannerTab, self).__init__(**kwargs)

                self.orientation = 'vertical'
                self.spacing = 10

            def load_tab(self):
                self.clear_widgets()
                self.add_widget(calendar_wid)

                day_view_slides = []
                for dayname in Time__.weekdaysFull:
                    day_view_slides.append({'wid': DayView(dayname=dayname), 'name': dayname, 'on_load': None})

                day_view_holder = TabsHolder(head_sizeX=.2, head_sizeY=0.175, slides=day_view_slides,
                                       head_fontSize=smid_font, font_name=light,
                                       head_background='white', inactive_color=(.45, .45, .45, .75),
                                       font_color=(1, 1, 1, 1), head_padding=(0, 0, 0, 0), head_spacing=0,
                                       active_color=(1, 1, 1, 1), active_fg=(0, 0, 0, 1))

                day_view_holder.select(Time__.dayLong)

                self.add_widget(day_view_holder)

        class DocumentsTab(BoxLayout):
            def __init__(self, **kwargs):
                super(DocumentsTab, self).__init__(**kwargs)

                def call_load_tab(button):
                    self.load_tab()

                # self.add_widget(Button(text='Access course notes from your peers and lecturers.',
                #                             color=(.1, .1, .1, 1), font_name=light, font_size=bigger_font,
                #                             on_release=call_load_tab))

            def load_tab(self):
                self.clear_widgets()

                def bring_note_writer(button):
                    global NotepadScreen_
                    try:
                        fprint(NotepadScreen_)
                    except:
                        NotepadScreen_ = NotepadScreen()
                        screenman_.add_widget(NotepadScreen())

                    screenman_.current = 'Notepad Screen'

                all_course_forms = membank.get('student_details')['student_details']['course_history']
                try:
                    this_semester_cf = all_course_forms['%s %s'%(this_semester, this_session)]
                except KeyError:
                    this_semester_cf = []

                class NoteTab(ScrollView):
                    def __init__(self, **kwargs):
                        super(NoteTab, self).__init__(**kwargs)

                        self.initialized = False

                        self.do_scroll_x = False
                        self.grid = GridLayout(cols=1, spacing=3)
                        self.grid.bind(minimum_height=self.grid.setter('height'))
                        self.add_widget(self.grid)

                        self.course_code = kwargs['course_code']

                    def initialize(self):
                        if self.initialized:
                            return

                        self.initialized = True
                        try:
                            documents = membank.get('documents')['documents']
                        except:
                            membank.put('documents', documents={})
                            documents = {}

                        # membank.put('documents', documents={})
                        documents = {}

                        fprint('Initalizing ')

                        download_color = get_color_from_hex(blue)
                        download_background_color = (1, 1, 1, 1)
                        download_in_progress_color = (.2, .2, .2, .4)
                        download_in_progress_background_color = (1, 1, 1, 1)
                        view_downloaded_color = (1, 1, 1, 1)
                        view_downloaded_background_color = get_color_from_hex(blue)

                        def open_note(button):
                            dict_ = eval(button.background_normal)
                            if os.path.exists(r'%s/%s' % (download_path, dict_['title'])):
                                ''

                            else:
                                def download_thread():
                                    button.text = 'DOWNLOADING...'
                                    button.color = download_in_progress_color
                                    button.background_color = download_in_progress_background_color

                                    notification.notify('ABUAD Portal',
                                                        '%s download in progress...' % dict_['title'].title(), {})

                                    webengine.download_blob(
                                        membank.get('student_details')['student_details']['matric_no'], dict_['weblink'])

                                    button.text = 'OPEN'
                                    button.color = view_downloaded_color
                                    button.background_color = view_downloaded_background_color
                                    notification.notify('ABUAD Portal',
                                                        '%s successfully downloaded!' % dict_['title'].title(), {})

                                threading.Timer(0, download_thread).start()

                        def get_documents():
                            nb_for_course = webengine.get_documents(
                                membank.get('student_details')['student_details']['matric_no'],
                                self.course_code)
                            documents[self.course_code] = nb_for_course

                            membank.put('documents', documents=documents)

                            def add_documents_to_grid(dt):
                                for nb_dict in nb_for_course:
                                    nb_title = nb_dict['title']
                                    nb_date = nb_dict['datetime']
                                    # nb_weblink = nb_dict['weblink']
                                    nb_author = nb_dict['author']

                                    download_button = Button(text='DOWNLOAD', font_name=bold, color=(.2, .2, .2, .5),
                                                             background_normal=str(nb_dict), font_size=small_font,
                                                             background_color=(1, 1, 1, 1), size_hint_x=.3,
                                                             pos_hint={'center_x': .65, 'center_y': .5},
                                                             on_release=open_note)

                                    if os.path.exists(r'%s/%s' % (download_path, nb_title)):
                                        download_button.text = 'OPEN'
                                        download_button.color = view_downloaded_color
                                        download_button.background_color = view_downloaded_background_color

                                    else:
                                        download_button.color = download_color
                                        download_button.background_color = download_background_color

                                    this_nb_bar = BoxLayout()
                                    this_nb_bar.add_widget(Button(
                                        text='[color=000000]%s[/color][size=%s]\n[color=%s][font=%s]%s[/font] | %s'
                                             '[/color][/size]' % (
                                            nb_title, small_font, subtext_color, bold, nb_author['name'], nb_date),
                                        font_name=light, font_size=big_font, background_normal=str(nb_dict),
                                        markup=True,
                                        pos_hint={'center_x': .5, 'center_y': .5},
                                        text_size=(.7*home_screen.the_tabs_holder.width, None)))
                                    this_nb_bar.add_widget(download_button)
                                    self.grid.add_widget(this_nb_bar)

                                self.grid.size_hint_y = len(self.grid.children) * .16
                                self.scroll_y += 0.0001

                            Clock.schedule_once(add_documents_to_grid, 1)

                        def write_stored_documents():
                            #code for writing stored documents
                            get_documents()

                        threading.Timer(0, write_stored_documents).start()

                courses_tabs = []
                for course_dict in this_semester_cf:
                    course_code = course_dict['code']
                    this_notetab = NoteTab(course_code=course_code)
                    courses_tabs.append({'wid': this_notetab, 'name': course_code, 'on_load': this_notetab.initialize})

                if len(this_semester_cf) == 0:
                    self.add_widget(Label(text='Register courses (under "Manage Courses"  on the left pane), '
                                               'for this semester, to start.', font_name=light, halign='center',
                                          text_size=(.7*Window.width, None), font_size=big_font, color=(0,0,0,1)))
                else:
                    self.add_widget(TabsHolder(head_sizeX=.25, head_sizeY=0.09, slides=courses_tabs,
                                               head_fontSize=mid_font, font_name=light,
                                               head_background='white', inactive_color=(.45, .45, .45, .55),
                                               font_color=(1, 1, 1, 1), active_color=get_color_from_hex(blue),
                                               active_fg=(1,1,1,1)))

        class ConversationTab(BoxLayout):
            def __init__(self, **kwargs):
                super(ConversationTab, self).__init__(**kwargs)

                self.orientation = 'vertical'

            def load_tab(self):
                self.clear_widgets()

                class Avatar(AsyncImage):
                    def __init__(self, **kwargs):
                        super(Avatar, self).__init__(**kwargs)

                        self.keep_ratio = False
                        self.allow_stretch = True

                        self.source = kwargs['fn']

                class IDCard(FloatLayout):
                    def __init__(self, **kwargs):
                        super(IDCard, self).__init__(**kwargs)

                        self.orientation = 'vertical'

                        dict_ = kwargs['dict_']
                        matric_no = dict_['matric_no']
                        fullname = dict_['fullname']
                        connection = dict_['connection']
                        profile_photo = dict_['profile_photo']

                        def show_chat_page(sender_wid):
                            global ChatScreen_
                            try:
                                fprint(ChatScreen_)
                            except NameError:
                                ChatScreen_ = ChatScreen()
                                screenman_.add_widget(ChatScreen_)

                            screenman_.current = 'Chat Screen'
                            ChatScreen_.reload(dict_)

                        name_plate = Button(
                            text='{fullname}[color=a0a0a0][size={size}]\n{connection}[/size][/color]'.format(
                                **{'fullname': fullname, 'connection': connection, 'size': small_font}),
                            background_normal='white', background_down='white', size_hint_y=1, color=(.2, .2, .2, 1),
                            text_size=(None, .25 * Window.height), font_name=light,
                            font_size=mid_font, pos_hint={'center_x':.5, 'center_y':.5}, markup=True,
                            on_release=show_chat_page, halign='center')

                        self.add_widget(name_plate)

                        profile_photo = Avatar(fn=profile_photo, pos_hint={'center_x': .5, 'center_y': .65},
                                               size_hint_y=.73, action=show_chat_page)
                        self.add_widget(profile_photo)

                class ChatStrip(BoxLayout):
                    def __init__(self, **kwargs):
                        super(ChatStrip, self).__init__(**kwargs)

                        self.fullname = kwargs['recipient']['fullname']
                        self.matric_no = kwargs['recipient']['matric_no']
                        self.recipient_dict = kwargs['recipient']['dict']
                        self.last_msg = kwargs['last_msg']
                        self.avatar_img_addr = kwargs['avatar_img_addr']

                        self.orientation = 'horizontal'
                        self.size_hint_y = .14

                        self.profile_photo = Avatar(fn=self.avatar_img_addr, size_hint_x=None, width=cm(2))
                        self.add_widget(self.profile_photo)

                        message_color = '786868'
                        message_font = light

                        if self.last_msg['status'] != 'read' and self.last_msg['recipient']['matric_no'] == membank.get(
                                'student_details')['student_details']['matric_no']:
                            message_color = '101010'
                            message_font = bold

                        self.msg_strip = Button(
                            text='[color=404040][font=%s]%s\n[/font][/color][color=%s][size=%s]%s[/size][/color]' % (
                                message_font, self.fullname, message_color, smid_font, self.last_msg['text']),
                            font_name=message_font, font_size=big_font, markup=True,
                            text_size=(.75*Window.width, None), background_normal='white',
                            pos_hint={'center_x':.5, 'center_y':.5})

                        self.time_label = Label(
                            text='2m ago', font_name=light, font_size=smid_font, color=(0, 0, 0, .5),
                            pos_hint={'center_x': .91, 'center_y': .8})

                        self.float_for_msg_strip = FloatLayout()
                        self.float_for_msg_strip.add_widget(self.msg_strip)
                        self.float_for_msg_strip.add_widget(self.time_label)

                        self.add_widget(self.float_for_msg_strip)

                    def write_last_msg(self, last_msg):
                        message_color = '786868'
                        message_font = light

                        if last_msg['status'] != 'read' and last_msg['recipient']['matric_no'] == membank.get(
                                'student_details')['student_details']['matric_no']:
                            message_color = '101010'
                            message_font = bold

                        self.msg_strip.text = '[color=404040][font=%s]%s\n[/font][/color][color=%s][size=%s]%s[/size]' \
                                              '[/color]' % (
                            message_font, self.fullname, message_color, smid_font, last_msg['text'])

                    def on_touch_down(self, touch):
                        if self.collide_point(touch.x, touch.y):
                            global ChatScreen_
                            try:
                                fprint(ChatScreen_)
                            except NameError:
                                ChatScreen_ = ChatScreen()
                                screenman_.add_widget(ChatScreen_)

                            screenman_.current = 'Chat Screen'
                            ChatScreen_.reload(self.recipient_dict)

                def refresh_suggestions():
                    suggestions = webengine.get_conversations_suggestion(
                        membank.get('student_details')['student_details']['matric_no'])

                    def add_suggestions_to_grid(suggestions):
                        for dict_ in suggestions:
                            sugg_grid.add_widget(IDCard(dict_=dict_))
                            sugg_grid.width += cm(6)
                            suggestions_scroll.size_hint_x += 0.000001
                            chat_scroll.size_hint_y += 0.0000001

                    Clock.schedule_once(lambda dt: add_suggestions_to_grid(suggestions), 0)

                # suggestions grid
                self.add_widget(Label(text='Suggestions', color=(0,0,0,.8), size_hint_y=.08))
                sugg_grid = GridLayout(rows=1, spacing=5, size_hint_x=None)
                sugg_grid.bind(minimum_height=sugg_grid.setter('height'))

                suggestions_scroll = ScrollView(do_scroll_y=False, size_hint=(1, 0.4))
                suggestions_scroll.add_widget(sugg_grid)

                threading.Timer(0, refresh_suggestions).start()

                already_added = {}

                # chat scroll
                chat_grid = GridLayout(cols=1, padding=5, spacing=10)
                chat_grid.bind(minimum_height=chat_grid.setter('height'))

                chat_grid.add_widget(suggestions_scroll)

                def write_chat_strips():
                    self.total_size_hint_y = .4

                    try:
                        stored_conversations = membank.get('stored_conversations')['stored_conversations']
                    except:
                        stored_conversations = {}

                    for recipient_matric_no in stored_conversations:
                        print('Writing for %s'%recipient_matric_no)
                        if len(stored_conversations[recipient_matric_no]) == 0:
                            continue

                        last_msg = stored_conversations[recipient_matric_no][-1]

                        if type(last_msg) is list:
                            continue

                        recipient_file = last_msg['recipient']
                        if last_msg['recipient']['matric_no'] == membank.get('student_details')[
                            'student_details']['matric_no']:
                            recipient_file = last_msg['sender']

                        recipient_file['dict'] = recipient_file
                        # print(last_msg)

                        if recipient_file['matric_no'] not in already_added:
                            # print('adding new')
                            this_chatstrip = ChatStrip(recipient=recipient_file, last_msg=last_msg,
                                                       avatar_img_addr=recipient_file['profile_photo'])
                            chat_grid.add_widget(this_chatstrip)
                            already_added[recipient_file['matric_no']] = this_chatstrip

                        else:
                            # print('write_last_msg')
                            already_added[recipient_file['matric_no']].write_last_msg(last_msg)

                        # print('')

                        self.total_size_hint_y += .14

                    chat_grid.size_hint_y = self.total_size_hint_y     # for chatstrips
                    # chat_grid.size_hint_y += .57                                    # for suggestions_scroll
                write_chat_strips()

                def get_all_unread_msgs():
                    unread_msgs = webengine.get_all_unread_messages(membank.get('student_details')[
                                                                        'student_details']['matric_no'])
                    membank.put('stored_conversations', stored_conversations=unread_msgs)

                    write_chat_strips()

                    if app_alive and home_screen.the_tabs_holder.get_current_tab() == 'Conversations':
                        threading.Timer(3, get_all_unread_msgs).start()

                threading.Timer(3, get_all_unread_msgs).start()

                chat_scroll = ScrollView(do_scroll_x=False)
                chat_scroll.add_widget(chat_grid)
                self.add_widget(chat_scroll)

                self.prev_added = []

        self.alerts_tab = AlertsTab()
        self.radar_tab = RadarTab()
        self.planner_tab = PlannerTab()
        self.documents_tab = DocumentsTab()
        self.messages_tab = ConversationTab()

        home_screen_slides = [{'wid': self.alerts_tab, 'name': 'Alerts', 'on_load': None},
                              {'wid': self.documents_tab, 'name': 'Documents', 'on_load': self.documents_tab.load_tab},
                              {'wid': self.planner_tab, 'name': 'Planner', 'on_load': self.planner_tab.load_tab},
                              {'wid': self.radar_tab, 'name': 'Radar', 'on_load': self.radar_tab.load_tab},
                              {'wid': self.messages_tab, 'name': 'Conversations', 'on_load': self.messages_tab.load_tab}]

        self.the_tabs_holder = TabsHolder(head_sizeX=.3, head_sizeY=0.09, slides=home_screen_slides,
                                          head_fontSize=mid_font, font_name=light,
                                          head_background='white', inactive_color=(.15, .15, .15, .85),
                                          font_color=(1, 1, 1, 1), head_padding=(0,0,0,0), head_spacing=0,
                                          active_color=get_color_from_hex(blue), active_fg=(1,1,1,1))

        self.productivity_pane = ProductivityPane()

        class PCLayout(BoxLayout):
            def __init__(self, **kwargs):
                super(PCLayout, self).__init__(**kwargs)

                home_screen.productivity_pane.size_hint_x = .2

                self.add_widget(home_screen.productivity_pane)
                self.add_widget(home_screen.the_tabs_holder)

        class MobileLayout(BoxLayout):
            def __init__(self, **kwargs):
                super(MobileLayout, self).__init__(**kwargs)

                home_screen.productivity_pane.size_hint_x = .8
                self.add_widget(home_screen.the_tabs_holder)

        def show_productivity_pane(self):
            def dismiss_pd(self):
                try:
                    home_screen.productivity_pane.parent.clear_widgets()
                except:
                    pass

                Popup.on_dismiss(self)

            global productivity_popup
            productivity_popup = the_popup = BlankPopup(size_hint=(.92, None), height=1.1 * Window.height,
                                   content=home_screen.productivity_pane, pos_hint={'center_x': 0, 'center_y': .5})
            the_popup.open()
            the_popup.bind(on_dismiss=dismiss_pd)

            anim = Animation(duration=.2, pos_hint={'center_x': .4, 'center_y': .5})
            anim.start(the_popup)

        student_details = membank.get('student_details')['student_details']
        fullname = student_details['fullname']
        dept = student_details['department']
        level = student_details['level']

        top_bar_float = FloatLayout(size_hint_y=.09)
        title_button = Button(text='[size=%s]%s[/size][font=assets/ROCK.ttf]\n%s, %s[/font]' % (
            big_font, fullname, dept, level), font_name=bold, color=(1, 1, 1, 1),
                              font_size=smid_font, markup=True, background_normal='white',
                              background_down='white', background_color=get_color_from_hex(d_blue),
                              pos_hint={'center_x': .5, 'center_y': .5}, text_size=(.92*Window.width, None),
                              on_release=show_productivity_pane)
        top_bar_float.add_widget(title_button)

        if mobile:
            title_button.text_size = (76*Window.width, None)
            top_bar_float.add_widget(AsyncImage(source='hamburger_icon.png', allow_stretch=True, keep_ratio=True,
                                                pos_hint={'center_x':.05, 'center_y':.5}, size_hint_y=.7))

        self.box.add_widget(top_bar_float)

        if not mobile:
            self.box.add_widget(PCLayout())
        else:
            self.box.add_widget(MobileLayout())


class MessageText(Button):
    def __init__(self, **kwargs):
        super(MessageText, self).__init__(**kwargs)

        fprint(Window.width)

        self.background_normal = 'white'
        self.font_name = light
        self.font_size = mid_font
        self.markup = True
        self.text_size = (.97 * Window.width, None)

        msg = kwargs['msg']

        sender = msg['sender']
        sender_matric_no = sender['matric_no']
        sender_fullname = sender['fullname']
        msg_text = str(msg['text'])
        msg_time = msg['time']

        if msg['recipient']['matric_no'] == membank.get('student_details')['student_details']['matric_no']:
            threading.Timer(0, lambda:webengine.send_read_receipt(
                membank.get('student_details')['student_details']['matric_no'], msg['id'])).start()

        self.line_limit = 70

        self.lines = len(msg_text) // self.line_limit
        if len(msg_text) % self.line_limit > 0:
            self.lines += 1

        self.size_hint_y = self.lines

        if sender_matric_no == membank.get('student_details')['student_details']['matric_no']:
            self.background_color = get_color_from_hex(blue)
            color_ = (1, 1, 1, 1)
            d_color = (.8, .8, .8, .5)

        else:
            self.background_color = (.8, .8, .8, 1)
            color_ = (0, 0, 0, 1)
            d_color = (.5, .5, .5, .5)

        msg_text = list(msg_text)
        for chr_index in range(0, len(msg_text), self.line_limit):
            msg_text[chr_index] = '\n%s' % msg_text[chr_index]
        msg_text = ''.join(msg_text)

        if msg_text.startswith('\n'):
            msg_text = msg_text[1:]

        self.text = '[color=%s]%s[/color][size=%s]\n[color=%s]%s, %s[/size][/color]' % (
            get_hex_from_color(color_), msg_text, small_font, get_hex_from_color(d_color), msg_time, sender_fullname)


class ChatScreen(Screen):
    def __init__(self, **kwargs):
        super(ChatScreen, self).__init__(**kwargs)

        self.name = 'Chat Screen'

        self.box = BoxLayout(orientation='vertical')
        self.add_widget(self.box)

        self.message_title_bar = Button(font_name=light, font_size=mid_font, markup=True,
                                        text_size=(.92 * Window.width, None), size_hint_y=.09)
        self.box.add_widget(self.message_title_bar)

        self.grid = GridLayout(cols=1, size_hint_y=1, spacing=2)
        self.grid.bind(minimum_height=self.grid.setter('height'))

        self.scroll = ScrollView(do_scroll_x=False, size_hint_y=.81)
        self.scroll.add_widget(self.grid)
        self.box.add_widget(self.scroll)

        new_msg_box = BoxLayout()

        global clear_chat_screen
        def clear_chat_screen():
            self.grid.clear_widgets()

        def bring_msg_p(self, instance):
            if instance:
                'slide upwards if on android'

                if mobile:
                    return
                    self.focus = False

                    new_msg_p = PeoplePopup(title='', content=new_msg_box, size_hint=(.5, .5))
                    new_msg_p.open()

            else:
                'slide downwards if on android'

        def send_a_msg(button):
            if self.msg_input.text == '':
                return

            threading.Timer(0, lambda:webengine.send_message(
                membank.get('student_details')['student_details']['matric_no'], self.recipient, self.msg_input.text,
                self.attachment)).start()

            def recycle(dt):
                self.msg_input.text = ''
                self.attachment = None
            Clock.schedule_once(recycle, 1)

        def take_photo():
            pass

        self.cam_input = AsyncImage(source='atlas://trail/select_cameraIcon', allow_stretch=True, keep_ratio=True,
                                    size_hint_x=.17, action=take_photo)

        self.msg_input = TextInput(hint_text='Type a message... ', font_name=light, font_size=big_font)
        self.msg_input.bind(focus=bring_msg_p)

        self.msg_send = AsyncImage(source='atlas://trail/sendIcon', allow_stretch=True, keep_ratio=True,
                                   size_hint_x=.17, action=send_a_msg)

        self.new_msg_bar = BoxLayout(size_hint_y=.09)
        self.new_msg_bar.add_widget(self.cam_input)
        self.new_msg_bar.add_widget(self.msg_input)
        self.new_msg_bar.add_widget(self.msg_send)
        self.box.add_widget(self.new_msg_bar)

    def reload(self, recipient_dict):
        self.grid.clear_widgets()

        self.written_msgs = []
        matric_no2 = recipient_dict['matric_no']

        self.recipient = matric_no2
        self.recipient_dict = recipient_dict

        connection_ = '...'
        try:
            connection_ = recipient_dict['connection']
        except:
            pass

        self.message_title_bar.text = '%s[size=%s]\n%s[/size]' % (recipient_dict['fullname'], small_font,
                                                                  connection_)

        self.msg_input.text = ''
        self.attachment = None

        try:
            stored_conversations = membank.get('stored_conversations')['stored_conversations']
        except:
            membank.put('stored_conversations', stored_conversations={})
            stored_conversations = {}

        try:
            conversation_with_user = stored_conversations[matric_no2]
        except:
            conversation_with_user = []

        written_msgs = []

        total_y = 0

        for msg in conversation_with_user:
            fprint('About to write msg "%s"' % msg)

            print(str(msg['text']))

            if str(msg['text']) == '' or (msg in written_msgs) or type(msg) is list:
                continue

            written_msgs.append(msg)

            this_msg_text = MessageText(msg=msg)
            self.grid.add_widget(this_msg_text)

            fprint(msg['text'], this_msg_text.lines)

            total_y += (this_msg_text.lines * .09)
            self.written_msgs.append(msg['id'])

        if len(self.grid.children) == 0:
            self.grid.add_widget(Widget())

        fprint('total_y = %s' % total_y)

        self.grid.size_hint_y = total_y
        self.scroll.size_hint_y += 0.0001
        self.scroll.scroll_y = 0

        self.allow_refresh = True
        threading.Timer(0, self.refresh_conversations).start()

    def refresh_conversations(self):
        if self.allow_refresh and app_alive:
            # code for getting last message
            last_unread = webengine.get_unread_messages(membank.get('student_details')['student_details']['matric_no'],
                                          self.recipient)
            # fprint(last_unread)

            try:
                stored_conversations = membank.get('stored_conversations')['stored_conversations']
            except:
                membank.put('stored_conversations', stored_conversations={})
                stored_conversations = {}

            try:
                stored_conversations[self.recipient].extend(last_unread)
            except:
                stored_conversations[self.recipient] = [last_unread]

            if stored_conversations != membank.get('stored_conversations')['stored_conversations']:
                Clock.schedule_once(lambda dt:
                                membank.put('stored_conversations', stored_conversations=stored_conversations), 0)

            for msg in last_unread:
                if msg['id'] not in self.written_msgs:
                    msg_text = MessageText(msg=msg)
                    self.grid.add_widget(msg_text)

                    # fprint(msg['text'], msg_text.lines)
                    self.grid.size_hint_y += (msg_text.lines * .09)
                    self.scroll.size_hint_y += 0.0001

                    self.written_msgs.append(msg['id'])

            threading.Timer(2, self.refresh_conversations).start()


class WelcomeScreen(Screen):
    def __init__(self, **kwargs):
        super(WelcomeScreen, self).__init__(**kwargs)

        welcomescreen = self

        self.name = 'Welcome Screen'

        self.scroll = ScrollView(do_scroll_x=False)
        self.add_widget(self.scroll)

        self.grid = GridLayout(cols=1, size_hint_y=1.5)
        self.grid.bind(minimum_height=self.grid.setter('height'))
        self.scroll.add_widget(self.grid)

        def add_home_screen():
            screenman_.add_widget(HomeScreen())
            screenman_.current = 'Home Screen'

        def do_login(button):
            matric_no = self.matric_no_input.text
            password = self.password_input.text

            def login_thread():
                submit_button.text = 'loading...'
                submit_button.background_color = (0, 0, 0, 0)

                ret_ = webengine.login(matric_no, password)

                submit_button.text = 'Login'
                submit_button.background_color = get_color_from_hex(d_gold)

                fprint('Login result: %s' % ret_)

                if 'error' in ret_:
                    Clock.schedule_once(toast('Error: %s' % ret_['error']), 1)
                else:
                    membank.put('student_details', student_details=ret_)

                    add_home_screen()

            threading.Timer(0, login_thread).start()

        def slide_layout_upwards(self, instance):
            if instance:
                welcomescreen.scroll.scroll_y = 0
            else:
                welcomescreen.scroll.scroll_y = 1

        def capitalize_text(self, value):
            self.text = value.upper()

        self.layout = FloatLayout(size_hint_y=1)
        self.layout.add_widget(Button(text='[font=%s][size=%s]ABUAD\nPortal[/size][/font]' % (college, font_scale(85)),
                                      background_normal='white', background_down='white',
                                      pos_hint={'center_x':.5, 'center_y':.5},
                                      background_color=get_color_from_hex(blue), halign='center', font_name=light,
                                      font_size=big_font, markup=True, text_size=(.8*Window.width, .7*Window.height),
                                      valign='top'))

        self.matric_no_input = TextInput(hint_text='Matric No. (ex. 11/ENG09/010)', font_name=light,
                                         font_size=bigger_font, size_hint=(.7, .075),
                                         pos_hint={'center_x': .5, 'center_y': .415},
                                         background_normal='white', background_active='white', multiline=False)
        self.password_input = TextInput(hint_text='Password', font_name=light, font_size=bigger_font, password=True,
                                        size_hint=(.7, .075), pos_hint={'center_x': .5, 'center_y': .312},
                                        background_normal='white', background_active='white', multiline=False)
        submit_button = Button(text='Login', font_name=light, font_size=big_font, background_normal='white',
                               background_color=get_color_from_hex(d_gold), size_hint=(.7, .075),
                               pos_hint={'center_x': .5, 'center_y': .21}, on_release=do_login)

        self.matric_no_input.bind(focus=slide_layout_upwards)
        self.matric_no_input.bind(text=capitalize_text)
        self.password_input.bind(focus=slide_layout_upwards)

        self.layout.add_widget(self.matric_no_input)
        self.layout.add_widget(self.password_input)
        self.layout.add_widget(submit_button)

        self.grid.add_widget(self.layout)
        self.grid.add_widget(Widget(size_hint_y=.5))


screenman_float = FloatLayout()
screenman_float.add_widget(Button(background_down='white', background_normal='white',
                                  background_color=Window.clearcolor, pos_hint={'center_x': .5, 'center_y': .5}))
screenman_float.add_widget(screenman_)


global toast
def toast(text_):
    this_toast = Button(text=text_, background_color=(0,0,0,.7), background_down='white', font_name=light,
                        text_size=(.8*Window.width, None), halign='center', pos_hint={'center_x':.5, 'center_y':.5},
                        background_normal='white', font_size=mid_font)
    screenman_float.add_widget(this_toast)

    def clear_toast(dt):
        screenman_float.remove_widget(this_toast)

    Clock.schedule_once(clear_toast, 2.5)


class AbuadPortalApp(App):
    def build(self):
        self.title = 'The ABUAD Portal'

        global cache
        cache = membank.get('cache')['cache']

        # screenman_.add_widget(WelcomeScreen())
        try:
            fprint(membank.get('student_details')['student_details'])
            if platform == 'win':
                screenman_.add_widget(SplashScreen())
            else:
                screenman_.add_widget(HomeScreen())
        except KeyError:
            screenman_.add_widget(WelcomeScreen())

        return screenman_float

    def _key_handler(self, *args):
        app__ = self
        key = args[1]
        if key in (1000, 27):
            try:
                import android
                android.hide_keyboard()
            except ImportError:
                pass

            try:
                if len(active_text_input) > 0:
                    fprint(active_text_input)
                    active_text_input[0].focus = False

                    global active_text_input
                    active_text_input = active_text_input[1:]
                    fprint('Stopping at activeTextInput')
                    return True
            except:
                pass

            try:
                if len(the_current_p) > 0:
                    fprint(the_current_p)
                    the_current_p[0].dismiss()
                    fprint('Stopping at popup')

                    global the_current_p
                    the_current_p = the_current_p[1:]
                    return True
            except:
                fprint(the_current_p)

            try:
                global screen_history
                screen_history = screen_history[:-1]
                screenman_.current = screen_history[-1]

                return True
            except ImportError:
                pass

            return True

    def on_pause(self):
        return True

    def on_start(self):
        if platform == 'android':
            import android
            android.map_key(android.KEYCODE_BACK, 1000)

        win = self._app_window
        win.bind(on_keyboard=self._key_handler)

        global app_alive
        app_alive = True

    def on_stop(self):
        global app_alive
        app_alive = False


AbuadPortalApp().run()
