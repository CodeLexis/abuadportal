import os
import sys
import urllib
import requests


global_ip = 'http://abuadportal-195119.appspot.com'
local_ip = 'http://localhost:8080'

if sys.platform == 'win32':
    servers_ip = global_ip ##feel free to change this to your localhost's port
else:
    servers_ip = global_ip   ## and this to the server's address

os.environ['SERVERS_IP'] = servers_ip

import apiclient


def get_user_file(matric_no):
    return {'matric no': '', 'department': '', 'college': '', 'level': '',
            'fullname': '', 'course forms': ''}
    pass


def get_cache():
    pass


def get_documents(matric_no, course):
    # get document_list from this semester course form, from the API
    return apiclient.get_documents(matric_no, course)
    # students_note = requests.get('%s/documents' % servers_ip, {'action': 'get documents', 'matric_no': matric_no,
    #                                                            'semester': semester}).json()
    # return students_note


def download_note(matric_no, note_id):
    pass


def download_blob(matric_no, address):
    # if address is not static, direct to apiclient.download_blob
    return bytes(requests.get(address).text)

# download_blob('', 'http://localhost:8080/blob?fn=Signals+and+Waves.pdf&content_type=file/pdf')


def get_conversations_suggestion(matric_no):
    suggestions = requests.get(
        '%s/conversations' % servers_ip,
        {'action': 'suggestions', 'matric_no': matric_no})
    return suggestions.json()


def get_unread_messages(matric_no, recipient):
    unread_messages = requests.get(
        '%s/conversations' % servers_ip,
        {'action': 'get undelivered', 'matric_no': matric_no,
         'recipient': recipient})
    return unread_messages.json()


def get_all_unread_messages(matric_no):
    unread_messages = requests.get('%s/conversations' % servers_ip,
                                   {'action': 'get all conversations',
                                    'matric_no': matric_no})
    return unread_messages.json()


def send_message(matric_no, recipient, text, attachment):
    unread_messages = requests.post('%s/conversations' % servers_ip,
                                   {'action': 'send', 'matric_no': matric_no,
                                    'recipient': recipient, 'text': text, 'attachment': attachment})
    return unread_messages.json()


def send_read_receipt(matric_no, msg_id):
    unread_messages = requests.post('%s/conversations' % servers_ip,
                                    {'action': 'send_read_receipt',
                                     'matric_no': matric_no, 'msg_id': msg_id})
    return unread_messages.json()


def get_user_data_col(matric_no, key_):
    pass


def get_study_tips():
    return requests.get('%s/study_tips' % servers_ip).json()


def get_announcements():
    demo_file = apiclient.get_announcements()
    return demo_file


def get_radar(matric_no):
    return requests.get('%s/radar' % servers_ip, {'request': 'get', 'matric_no': matric_no}).json()


def add_radar_post(matric_no, title, description, link, cover_img_path):
    return requests.post('%s/radar' % servers_ip, {'title': title, 'description': description, 'link': link,
                                                   'cover_img_path': cover_img_path}).json()


def like_radar_post(matric_no, id):
    return requests.get('%s/radar' % servers_ip, {'request': 'like', 'id': id, 'matric_no': matric_no}).json()


def get_assignment(course, semester_session):
    demo_file = apiclient.get_assignments(course, semester_session)
    return demo_file


def get_socialevents():
    return [{'datetime': '', 'location': '', 'description': ''}, {'datetime': '', 'location': '', 'description': ''}]
    pass


def create_user(matric_no, fullname, department, college):
    return requests.get('%s/create_user' % servers_ip,
                        {'matric_no': matric_no, 'fullname': fullname, 'department': department, 'college': college}).json()


def register_user(matric_no, fullname, department, college):
    return requests.get(servers_ip,
                        {'matric_no': matric_no, 'fullname': fullname, 'deparment': department, 'college': college})


def get_course_tree(mat_no, sem_session):
    course_tree = apiclient.get_course_tree(mat_no, sem_session)
    return course_tree


def register_courses(mat_no, selected_courses, sem_session):
    course_tree = apiclient.register_courses(mat_no, str(selected_courses), sem_session)
    return course_tree


def get_course_form(mat_no, sem_session):
    course_tree = apiclient.get_course_form(mat_no, str(selected_courses), sem_session)
    return course_tree


def login(matric_no, password):
    # get demo_file from their API, if the matric_no and passwords match
    demo_file = apiclient.get_user_file(matric_no, password)

    return demo_file


print(login('12/SCI14/000', 'passworder'))