import os
import requests


api_address = '%s/api' % os.environ['SERVERS_IP']


def get_user_file(matric_no, password):
    return requests.get(api_address, {'request': 'get_user_file', 'matric_no': matric_no, 'password': password}).text


def get_documents(matric_no, course):
    return requests.get(api_address, {'request': 'get_documents', 'matric_no': matric_no, 'course': course}).json()


def get_course_tree(mat_no, sem_session):
    return requests.get(api_address, {'request': 'get_course_tree', 'matric_no': mat_no,
                                      'sem_session': sem_session}).json()


def register_courses(matric_no, selected_courses, sem_session):
    return requests.get(api_address, {'request': 'register_courses', 'selected_courses': selected_courses,
                                      'sem_session': sem_session, 'matric_no': matric_no}).json()


def get_assignments(course, semester_session):
    return requests.get(api_address, {'request': 'get_assignments', 'semester_session': semester_session,
                                      'course': course}).json()


def get_announcements():
    return requests.get(api_address, {'request': 'get_announcements'}).json()


def download_blob(address):
    return requests.get(api_address, {'request': 'download_blob', 'address': address}).json()
