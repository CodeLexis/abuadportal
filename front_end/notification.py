import random
import sys

try:
    from jnius import autoclass
    # from android import activity as AndroidActivity

    Context = autoclass('android.content.Context')
    Intent = autoclass('android.content.Intent')
    PendingIntent = autoclass('android.app.PendingIntent')
    SystemClock = autoclass('android.os.SystemClock')
    AlarmManager = autoclass('android.app.AlarmManager')
    AndroidString = autoclass('java.lang.String')
    NotificationBuilder = autoclass('android.app.Notification$Builder')
    Uri = autoclass('android.net.Uri')

    service = autoclass('org.renpy.android.PythonActivity').mActivity
    # service.stopForeground(True)
except:
    pass


class Notification(object):
    def __init__(self, **kwargs):
        self.sound = kwargs['sound']

    def notify(self, title, message, dict):
        if sys.platform == 'win32':
            from win10toast import ToastNotifier
            toaster = ToastNotifier()
            toaster.show_toast(title, message, 'appIconNoShadow.ico', 6)
            # fprint('Notification: %s %s'%(title, message))
            return

        try:
            from plyer import vibrator
            vibrator.vibrate(0.65)
        except:
            None

        self.sound.play()

        # Logger.info('Notification: Starting %s, %s, %s' % (title, message, dict))
        Drawable = autoclass("{}.R$drawable".format(service.getPackageName()))

        notification_service = service.getSystemService(Context.NOTIFICATION_SERVICE)
        icon = getattr(Drawable, 'icon')

        notification_builder = NotificationBuilder(service)

        title = AndroidString(title.encode('utf-8'))
        message = AndroidString(message.encode('utf-8'))

        # Logger.info('Notification: 51')
        NotificationResponse = autoclass('org.renpy.android.PythonActivity')  # org.codelexis.svcs.NotificationResponse

        # Logger.info('Notification: 53')
        java_class = autoclass('org.renpy.android.PythonActivity').mActivity.getClass()  # service.getClass()
        # Logger.info('Notification: 55')
        try:
            notificationResponseClass = NotificationResponse().getClass()
            # Logger.info('Notification: 56')
        except:
            notificationResponseClass = java_class

        notificationIntent = Intent(service,
                                    notificationResponseClass)  # autoclass('org.renpy.android.PythonActivity')().getClass())
        # Logger.info('Notification: 57')
        notificationIntent.setAction(Intent.ACTION_VIEW)
        # Logger.info('Notification: 59')
        # notificationIntent.setData(str(dict))
        # Logger.info('Notification: 61')
        notificationIntent.setFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)

        notificationIntent.putExtra('url', str(dict))

        # Logger.info('Notification: 65')
        intent = PendingIntent.getActivity(service, 0, notificationIntent, 0)

        notification_builder.setContentTitle(title)
        notification_builder.setContentText(message)
        notification_builder.setContentIntent(intent)

        notification_builder.setSmallIcon(icon)
        notification_builder.setAutoCancel(True)
        notification_service.notify(random.randint(0, 100), notification_builder.build())
